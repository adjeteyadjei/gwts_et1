'use strict';

var tables = [];
angular.module('MobileApp.config', ['Schema'])
    .constant('DB_CONFIG', function(Schema){
        return {
            name: 'GWTSDBtest',
            tables: function() {
                angular.forEach(Schema, function(table) {
                    return {
                        name: table.name,
                        columns: function() {
                            var tableColumns = [{
                                name: 'oid',
                                type: 'integer primary key'
                            }];
                            return [
                                angular.forEach(table.properties, function(key, value) {
                                    this.push({
                                        name: value,
                                        type: 'text'
                                    });
                                }, tableColumns)
                            ]
                        }
                    }
                })
            }
        }
    });

/*
angular.module('MobileApp.config', ['Schema'])
.constant('DB_CONFIG', {
    name: 'DB',
    tables: [
      {
            name: 'documents',
            columns: [
                {name: 'id', type: 'integer primary key'},
                {name: 'title', type: 'text'},
                {name: 'keywords', type: 'text'},
                {name: 'version', type: 'integer'},
                {name: 'release_date', type: 'text'},
                {name: 'filename', type: 'text'},
                {name: 'context', type: 'text'}
            ]
        }
    ]
});
*/