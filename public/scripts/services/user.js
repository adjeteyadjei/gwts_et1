'use strict';

/**
 * @ngdoc service
 * @name mobileAppApp.user
 * @description
 * # user
 * Service in the mobileAppApp.
 */
angular.module('MobileApp.services')
	.service('User', function User(storage) {
		return {
			get: function() {
				return storage.get('User');
			},
			set: function(user) {
				storage.set('User', user);
			},
			delete: function() {
				this.set(null);
			}
		}
	});