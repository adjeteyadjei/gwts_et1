'use strict';

/**
 * @ngdoc service
 * @name mobileAppApp.db
 * @description
 * # db
 * Service in the mobileAppApp.
 */
angular.module('MobileApp.services')

.factory('pouchdb', function() {
	var db = new PouchDB('dataMan');
	window.ddd = db;
	return db;
})

.factory('Model', function Model($q, pouchdb, User) {

	function Model(modelType, pluralType) {
		this.modelType = modelType;
		this.pluralType = pluralType || modelType + 's';
	}

	// this is annoyingly buggy
	Model.prototype.isEmpty = function(callback) {
		this.all().then(function(res) {
			var result = (res.length > 0) ? false : true
			callback(result);
			//return ;
		});
	}

	Model.prototype.all = function(options, callback) {
		var modelType = this.modelType;

		function map(doc, emit) {
			if (doc.modelType === modelType) {
				emit(null, doc);
			}
		}
		return Model.getView(map, options, callback);
	};

	Model.prototype.find = function(filter, options, callback) {
		var modelType = this.modelType;
		var key = Object.keys(filter)[0];
		var value = filter[key];

		function map(doc, emit) {
			if (doc.modelType === modelType && doc[key] === value) {
				emit(null, doc);
			}
		}
		return Model.getView(map, options, callback);
	};

	Model.prototype.get = function(id, callback) {
		return Model.get(id, callback);
	};

	Model.prototype.create = function(doc, callback) {
		doc.modelType = this.modelType;
		return Model.create(doc, callback);
	};

	Model.prototype.update = function(doc, callback) {
		return Model.update(doc, callback);
	};

	Model.prototype.save = function(doc, callback) {
		if (doc._id) {
			//Update
			return Model.update(doc, callback);
		} else {
			//Create
			doc.modelType = this.modelType;
			return Model.create(doc, callback);
		};
	};

	Model.prototype.delete = function(doc, callback) {
		return Model.delete(doc, callback);
	};

	Model.prototype.post = function(data, addOn) {
		var modelType = this.modelType;
		var recs = data.map(function(record) {
			record.modelType = modelType;
			if (addOn) {
				var key = Object.keys(addOn)[0];
				var value = addOn[key];
				record[key] = value;
			};
			return record;
		});
		pouchdb.bulkDocs(recs, function(err, response) {
			return response;
		});
	};

	Model.prototype.drop = function(data) {
		var recs = data.map(function(record) {
			record._deleted = true;
			return record;
		});
		pouchdb.bulkDocs(recs, function(err, response) {
			return response;
		});
	};

	Model.getView = function(viewName, options, callback) {
		var d = $q.defer();
		if (typeof options !== 'object') {
			options = {};
		}

		pouchdb.query(viewName, options).then(function(results) {
			var docs = _.map(results.rows, function(doc) {
				return doc.value;
			});
			d.resolve(docs);
			if (callback) {
				callback(docs);
			}
		}).catch(function(err) {
			d.reject(err);
		});
		return d.promise;
	};

	Model.get = function(id, callback) {
		var d = $q.defer();
		pouchdb.get(id).then(function(result) {
			d.resolve(result);
			if (callback) {
				callback(result);
			}
		}).catch(function(err) {
			d.reject(err);
		});
		return d.promise;
	};

	Model.create = function(doc, callback) {
		var d = $q.defer();
		var timestamp = new Date().getTime();
		//doc.userid = User.get().id; //Todo: Uncomment this when login is done
		pouchdb.post(doc).then(function(result) {
			d.resolve(result);
			if (callback) {
				callback(result);
			}
		}).catch(function(err) {
			d.reject(err);
		});
		return d.promise;
	};

	Model.update = function(doc, callback) {
		var d = $q.defer();
		//var timestamp = new Date().getTime();
		//doc.updatedAt = timestamp;
		//doc.userid = User.get().id; //Todo: Uncomment this when login is done
		pouchdb.put(doc).then(function(result) {
			d.resolve(result);
			if (callback) {
				callback(result);
			}
		}).catch(function(err) {
			d.reject(err);
		});
		return d.promise;
	};

	Model.delete = function(doc, callback) {
		var d = $q.defer();
		pouchdb.remove(doc).then(function(result) {
			d.resolve(result);
			if (callback) {
				callback(result);
			}
		}).catch(function(err) {
			d.reject(err);
		});
		return d.promise;
	};

	return (Model);


});