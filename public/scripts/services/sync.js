'use strict';

/**
 * @ngdoc service
 * @name MobileApp.services.sync
 * @description
 * # sync
 * Service in the MobileApp.services.
 */
angular.module('MobileApp.services')
  .service('Sync', function Sync(pouchdb, $q, Schema, Adapter, StriplineItems) {
  	return{
  		pull: function (model) {
  			var d = $q.defer();
  			Adapter.query(model).then(function(res) {
  				return d.resolve(res);
  			}).catch(function (error) {
  				d.reject(error);
  			});
  			return d.promise;
  		},
  		push: function (model, data, ignoreFields) {
  			var d = $q.defer();
        Adapter.create(model, data, ignoreFields).then(function(res) {
          return d.resolve(res);
        }).catch(function (error) {
          d.reject(error);
        });
        return d.promise;
  		},
      // Sync Center
      syncStriplineItem: function (record){
        var d = $q.defer();
        var fieldsToIgnore = ["_id", "_index", "isDirty","latestEdition"];
        var stash = angular.copy(record);
        this.push('stripline_item', record, fieldsToIgnore).then(function(res) {
          if (res.status){
            if (res.data.status){
              stash.isDirty = false;
              stash.oid = res.data.data.changedObjects[0].savedOid 
              stash.latestEdition = res.data.data.changedObjects[0].savedEdition 
              StriplineItems.save(stash);
              console.log('Record after saved',stash);
              d.resolve(stash);
            } else{
              d.reject("Stock No: " + record.stockNo + " -> " + res.data.errorMessage);
            }
          }else{
            d.reject("Error from the server");
          }
        },function(err){
          d.reject("Stock No: " + record.stockNo + " -> " + err);
        });
        return d.promise;
      },
      // Sync Center
      syncStriplineItemCheck: function (record){
        var d = $q.defer();
        var fieldsToIgnore = ["_id", "_index", "isDirty","latestEdition"];
        var stash = angular.copy(record);
        this.push('check_stripline_item', record, fieldsToIgnore).then(function(res) {
          if (res.status){
            if (res.data.status){
              stash.isDirty = false;
              stash.oid = res.data.data.changedObjects[0].savedOid 
              stash.latestEdition = res.data.data.changedObjects[0].savedEdition 
              StriplineItems.save(stash);
              console.log('Record after saved',stash);
              d.resolve(stash);
            } else{
              d.reject("Stock No: " + record.stockNo + " -> " + res.data.errorMessage);
            }
          }else{
            d.reject("Error from the server");
          }
        },function(err){
          d.reject("Stock No: " + record.stockNo + " -> " + err);
        });
        return d.promise;
      },
      syncTIFs: function(record){
        var d = $q.defer();
        var fieldsToIgnore = ["_id", "_index", "generateId", "certificate",
          "compartment", "totalVolume", "rSWtreeInformationLine.boleVolume",
          "rSWtreeInformationLine.db", "rSWtreeInformationLine.dt", "rSWtreeInformationLine.volume",
          "rSWtreeInformationLine.totalVolume", "rSWtreeInformationLine.specie", "rSWtreeInformationLine.measurement"
        ];
        //var stash = angular.copy(record)
        this.push('tif', record, fieldsToIgnore).then(function(res) {
          if (res.status) {
            if (res.data.status) {
              //Tifs.delete(record);
              d.resolve(record);
            } else {
              d.reject("TIF No: " + record.no + " -> " + res.data.errorMessage);
            }
          }
        },function(err){
          d.reject("TIF No: " + record.no + " -> " +err);
        });
        return d.promise;
      },
      syncLMCCs: function(record){
        var d = $q.defer();
        var fieldsToIgnore = ["_id", "_index", "generateId", "certificate",
          "compartment", "totalVolume", "expireDate", "rSWforestCertCompartment",
          "rSWforestCertificate", "rSWdeliveryDocketLine.db", "rSWdeliveryDocketLine.dt",
          "rSWdeliveryDocketLine.volume", "destination", "rSWdeliveryDocketLine.measurement",
          "rSWdeliveryDocketLine.totalVolume", "rSWdeliveryDocketLine.specie"
        ];
        var stash = angular.copy(record)
        this.push('lmcc', record, fieldsToIgnore).then(function(res) {
          if (res.status) {
            if (res.data.status) {
              Lmccs.delete(stash);
              d.resolve(stash);
            } else {
              d.reject("LMCC No: " + record.no + " -> " + res.data.errorMessage);
            }
          }
        },function(err){
          d.reject("LMCC No: " + record.no + " -> " +err);
        });
        return d.promise;
      },
      syncLIFs: function(record){
        var d = $q.defer();
        var fieldsToIgnore = ["_id", "_index", "generateId", "certificate",
          "compartment", "totalVolume", "expireDate", "rSWforestCertCompartment",
          "rSWforestCertificate", "rSWlogInformationLine.db", "rSWlogInformationLine.dt",
          "rSWlogInformationLine.volume", "destination", "rSWlogInformationLine.measurement",
          "rSWlogInformationLine.totalVolume", "rSWlogInformationLine.specie"
        ];
        var stash = angular.copy(record)
        this.push('lif', record, fieldsToIgnore).then(function(res) {
          if (res.status) {
            if (res.data.status) {
              Lifs.delete(stash);
              d.resolve(stash);
            } else {
              d.reject("LIF No: " + record.no + " -> " + res.data.errorMessage);
            }
          }
        },function(err){
          d.reject("LIF No: " + record.no + " -> " +err);
        });
        return d.promise;
      }
  	};

  });
