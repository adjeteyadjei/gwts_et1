'use strict';

/**
 * @ngdoc service
 * @name MobileApp.services.store
 * @author olonka
 * @description localStorage Store(model) class and dbContext
 * Allow us to create instance of a store which links to a local stoarge record key
 * # store
 * Factory in the MobileApp.services.
 */
angular.module('MobileApp.services')
  .factory('Store', function Store(storage, User) {

    //Store constructor. Creates a key with the pluralize name
    function Store(type) {
      //this._type represents the record key
      this._type = endsWith(type) ? type : type + 's';
      //Create an empty value record with the key if it doesn't exist 
      if (!storage.get(this._type)) storage.set(this._type, []);
    }

    function endsWith(str) {
      return str.indexOf('s', str.length - 's'.length) !== -1;
    }

    //Return all records in store
    Store.prototype.all = function() {
      var objs = Store.query(this._type);
      objs = objs.map(function(obj, i) {
        obj._index = i;
        return obj;
      })
      return objs;
    };

    //Filter records in store.
    //Params (filter) {prop: value}
    //Can currently take object with one property
    Store.prototype.find = function(filter) {
      return Store.query(this._type, filter);
    };

    Store.prototype.findById = function(id) {
      return Store.query(this._type, {
        _id: id
      })[0];
    };

    Store.prototype.findByIndex = function(index) {
      var obj = storage.get(this._type)[index];
      if (obj) {
        obj._index = index
      };
      return obj;
    };

    Store.prototype.save = function(obj) {
      var dbSet = storage.get(this._type);
      var res;
      //obj.userid = User.get().oid;
      try {
        if (obj._id) {
          //Update Object
          /*if(!_.isEqual(dbSet[obj._index],obj)){ //Proposed: This will add an isDirty property to every data update that does not correspond with earlier one
            obj.isDirty = true;
          }*/
          dbSet[obj._index] = obj;
          res = storage.set(this._type, dbSet);
        } else {
          //Create New Object
          obj._id = new Date().getTime();
          dbSet.push(obj);
          storage.set(this._type, dbSet);
        };
        return {
          _id: obj._id,
          success: true,
          message: 'Record saved successfully.'
        };
      } catch (err) {
        return {
          success: false,
          message: err.message
        };
      }

    };

    //Bulk Save.
    Store.prototype.post = function(objs, addOn) {
      var dbSet = storage.get(this._type);
      objs = objs.map(function(obj, i) {
        obj._id = new Date().getTime() + i;
        if (addOn) {
          var keys = Object.keys(addOn);
          keys.forEach(function (k) {
            obj[k] = addOn[k];
          });
        };
        return obj;
      })
      dbSet.push.apply(dbSet, objs);
      storage.set(this._type, dbSet);
    };

    Store.prototype.delete = function(obj) {
      try {
        var dbSet = storage.get(this._type);
        dbSet.splice(obj._index, 1);
        storage.set(this._type, dbSet);
        return {
          success: true,
          message: "Record Deleted."
        };
      } catch (err) {
        return {
          success: false,
          message: err.message
        };
      }

    };

    Store.prototype.drop = function() {
      storage.set(this._type, []);
    };

    Store.prototype.exist = function() {
      var dbSet = storage.get(this._type);
      return (dbSet);
    };

    Store.prototype.count = function() {
      var dbSet = storage.get(this._type);
      return dbSet.length;
    };

    Store.prototype.hasData = function() {
      var dbSet = storage.get(this._type);
      return (dbSet.length > 0);
    };

    Store.query = function(storeType, filter) {
      var dbSet = storage.get(storeType);
      if (!filter) return dbSet;

      var data = _.where(dbSet, filter);
      data = data.map(function(obj, i) {
        obj._index = _.indexOf(dbSet, obj);
        return obj;
      });

      return data;
    };

    return (Store);
  })
//Store Instances for models
.factory('Forests', function(Store) {
  return new Store('forest');
})
  .factory('Species', function(Store) {
    return new Store('species');
  })
  .factory('Locations', function(Store) {
    return new Store('location');
  })
  .factory('Landowners', function(Store) {
    return new Store('landowner');
  })
  .factory('Certificates', function(Store) {
    return new Store('certificate');
  })
  .factory('PlantationPermits', function(Store) {
    return new Store('plantationPermit');
  })
  .factory('Compartments', function(Store) {
    return new Store('compartment');
  })
  .factory('Tifs', function(Store) {
    return new Store('tif');
  })
  .factory('Ppcs', function(Store) {
    return new Store('ppc');
  })
  .factory('Lifs', function(Store) {
    return new Store('lif');
  })
  .factory('Lmccs', function(Store) {
    return new Store('lmcc');
  })
  .factory('Plmccs', function(Store) {
    return new Store('lmcc');
  })
  .factory('Dockets', function(Store) {
    return new Store('docket');
  })
  .factory('StockSurvey', function(Store) {
    return new Store('stockSurvey');
  })
  .factory('Striplines', function(Store) {
    return new Store('striplines');
  })
  .factory('StriplineItems', function(Store) {
    return new Store('striplineItems');
  })
  .factory('CheckSurvey', function(Store) {
    return new Store('CheckSurvey');
  })
  .factory('CheckStripLines', function(Store) {
    return new Store('CheckStripLines');
  })
  .factory('CheckStripLineItems', function(Store) {
    return new Store('CheckStripLineItems');
  });