'use strict';

angular.module('MobileApp.services')
	.service('Sqldb', function Sqldb($q, Schema) {
		var self = this;
		self.db = null;

		/* 
			ENSURE DATABASE IS CREATED AND ALL TABLES SET UP
    */
		var DB_name = 'myDB';
		var CONFIG_TABLES = generateTables();

		self.init = function() {
			// Use self.db = window.sqlitePlugin.openDatabase({name: DB_name}); in production
			self.db = window.sqlitePlugin.openDatabase(DB_name+".db", '1.0', 'database', -1);
			angular.forEach(CONFIG_TABLES, function(table) {
				self.create(table.name, table.columns);
			});
		};

		// Builds all columns for a specific table using the table properties from the schema
		function generateTableColumns(properties) {
			var tableColumns = [{
				name: 'oid',
				type: 'integer primary key'
			},{
				name: 'latestEdition',
				type: 'integer'
			}];
			angular.forEach(properties, function(key, value) {
				var colType = (value.substr((value.length - 3),3) == "Oid") ? "integer" : "text";
				this.push({
					name: value,
					type: colType
				});
			}, tableColumns);
			return tableColumns;
		}

		//Function to generate table structure
		function generateTables() {
			var dataTables = [];
			angular.forEach(Schema, function(key,value) {
				this.push({
					name: value,
					columns: generateTableColumns(Schema[value].properties)
				});
			}, dataTables);
			return dataTables;
		}


		self.create = function(tablename, dataColumns) {
			var columns = [];
			angular.forEach(dataColumns, function(column) {
				columns.push(column.name + ' ' + column.type);
			});
			var query = 'CREATE TABLE IF NOT EXISTS ' + tablename + ' (' + columns.join(',') + ')';
			self.db.transaction(function(tx){
				tx.executeSql(query);
			});
		}

		self.query = function(query, bindings) {
			bindings = typeof bindings !== 'undefined' ? bindings : [];
			var deferred = $q.defer();
			self.db.transaction(function(transaction) {
				transaction.executeSql(query, bindings, function(transaction, result) {
					console.log(result);
					deferred.resolve(result);
				}, function(transaction, error) {
					deferred.reject(error);
				});
			});
			return deferred.promise;
		};

		//todo: finish batch insert statement
		self.insert = function(table, data) {
			var columns = [];
			var ques = []
			angular.forEach(Schema[table].properties,function(key,value){
				this.push(value);
				ques.push(value);
			},columns);
			self.db.transaction(function(tx) {
				var sql = "INSERT INTO table ("+columns.join(',')+") VALUES ("+ques+")";
				angular.forEach(data,function(row){
					var rowData = [];
					angular.forEach(row,function(value){
						this.push(value);
					},rowData);

					tx.executeSql(sql,rowData,
						function(tx, res) {
				     console.log("insertId: " + res.insertId + " -- probably 1");
				     console.log("rowsAffected: " + res.rowsAffected + " -- should be 1");
				     db.transaction(function(tx) {
				       tx.executeSql("select count(id) as cnt from test_table;", [], function(tx, res) {
				         console.log("res.rows.length: " + res.rows.length + " -- should be 1");
				         console.log("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");
				       });
				     });
				   }, function(e) {
				     console.log("ERROR: " + e.message);
				   });
				});
		  });
		};

		function onSuccess(resultSet) {
		    console.log('Query completed: ' + JSON.stringify(resultSet));
		}

		function onError(error) {
		    console.log('Query failed: ' + error);
		}


		self.fetchAll = function(result) {
			var output = [];
			for (var i = 0; i < result.rows.length; i++) {
				output.push(result.rows.item(i));
			}
			return output;
		};

		self.fetch = function(result) {
			return result.rows.item(0);
		};

		self.all = function(table) {
			return self.query('SELECT * FROM ' + table)
				.then(function(result) {
					return self.fetchAll(result);
				});
		};

		self.getById = function(table, id) {
			return self.query('SELECT * FROM ' + table + ' WHERE id = ?', [id])
				.then(function(result) {
					return self.fetch(result);
				});
		};

		self.exist = function(table){
			return self.query("SELECT name FROM sqlite_master WHERE type='table' AND name='" + table + "'")
			.then(function(result){
				return (result) ? true : false
			});
		}

		self.inspectTable = function(table){
			db.executeSql("pragma table_info (table);", [], function(res) {
	      console.log("PRAGMA res: " + JSON.stringify(res));
	    });
		}

		self.count = function(table){
			db.transaction(function(tx) {
        tx.executeSql("select count(id) as cnt from table;", [], function(tx, res) {
          return res.rows.length;
        });
      });
		}

		return self;
	})
// Resource service example