'use strict';

/**
 * @ngdoc service
 * @name mobileAppApp.appConfif
 * @description
 * # appConfif
 * Service in the mobileAppApp.
 */
angular.module('MobileApp.services')
	.service('AppConfig', function AppConfig(storage) {

		function store(key, valueObj) {
			//Set
			if (valueObj) {
				storage.set(key, valueObj);
				return valueObj;
			};
			//Get
			var result = storage.get(key);
			return result;
		}

		return {
			activate: function(code) {
				return store('ActivationCode', code);
			},
			isActivated: function() {
				return store('ActivationCode') ? true : false;
			},
			isSetUp:function () {
				return store('SetUp') ? true : false;
			},
			syncAddress: function(newAddress) {
				return store('SyncAddress', newAddress);
			},
			dock: function(dock) {
				return store('Dock', dock);
			},
			forest: function(newForest) {
				return store('Forest', newForest);
			},
			supervisor: function(supervisor) {
				return store('Supervisor', supervisor);
			},
			operation: function(op) {
				return store('Operation', op);
			},
			getForestCode:function(){
				return store('Forest').code
			},
			getForestType:function(){
				return store('Forest').type
			},
			getForestOid:function(){
				return store('Forest').oid
			},
			getForestName:function(){
				return store('Forest').name
			}
		}
	});