'use strict';
var app = angular.module('MobileApp.services').service('Authenticate',
  function Authenticate($http, User, $location, WTSURL,$state) {

    var currentUser = {};
    return {
      setCurrentUser: function(user) {
        User.set(user);
        currentUser = user;
      },
      currentUser: function() {
        return currentUser;
      },
      logOut: function() {
        User.delete();
        $location.path('/login');
      },
      secure: function() {
        var cookieVal = User.get();
        if (!cookieVal) {
          this.currentUser = {};
          //$location.path('/login');
          return false;
        } else {
          this.currentUser = cookieVal;
          return true;
        }
      },
      userLogin: function(user) {
        var data = {};
        data.command = 'ValidateUser';
        data.parameters = {
          userid: user.username,
          password: user.password
        };
        //data.parameters.properties = 'name';
        return $http.post(WTSURL, data);
      },
      adminLogin: function(user) {
        var data = {};
        data.command = 'ValidateUser';
        data.parameters = {
          userid: user.username,
          password: user.password
        };
        return $http.post(WTSURL, data);
      }
    };
  }
);