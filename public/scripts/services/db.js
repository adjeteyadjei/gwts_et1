'use strict';

/**
 * @ngdoc service
 * @name mobileAppApp.db
 * @description
 * # db
 * Service in the mobileAppApp.
 */
angular.module('MobileApp.services')
  .service('db', function db(storage) {
    return {
      get: function(store, index) {
        //Get Record by Index or by all
        if(index){
          return storage.get(store)[index];
        }
        console.log('Data loaded : ' + store); //debug
        return storage.get(store);
      },
      query: function(store, prop, value) {
        //Get Record by Id
        var results = storage.get(store);
        if (prop && value) {
          results = results.filter(function(item) {
            return item[prop] === value;
          });
        }
        return results;
      },
      create: function(store, data) {
        var dbset = storage.get(store);
        if (!dbset) dbset = [];
        data.id = new Date().getTime();
        data.latestEdition = 0;
        data.modified = true;
        dbset.push(data);
        storage.set(store, dbset);
        console.log('New Model Created : ' + store);
      },
      update: function(store, index, data) {
        var dbset = storage.get(store);
        data.id = dbset[index].id;
        dbset[index] = data;
        data.modified = true;
        storage.set(store, dbset);
      },
      delete: function(store, index) {
        var dbset = storage.get(store);
        dbset.splice(index, 1);
        storage.set(store, dbset);
      },
      changes: function(store) {
        var data = {
          created: [],
          updated: [],
          deleted: []
        };
        return data;
      },
      exist: function(store) {
        var dbset = storage.get(store);
        return (dbset);
      },
      isEmpty:function(store){
        return (this.get(store) == null);
      },
      reset: function(){
        return storage.clearAll();
      }
    };
  });