'use strict';

/**
 * @ngdoc service
 * @name mobileAppApp.helpers
 * @description
 * # helpers
 * Factory in the mobileAppApp.
 */
angular.module('MobileApp.services')
  .factory('MSG', function($ionicPopup, $q) {
    return {
      show: function(title, message) {
        var alertPopup = $ionicPopup.alert({
          title: title,
          template: message
        });
        alertPopup.then(function(res) {});
      },
      alert: function(message) {
        this.show("Alert", message);
      },
      error: function(message) {
        this.show("Error", message);
      },
      success: function(message) {
        this.show("Success", message);
      },
      confirm: function(title, message) {
        var d = $q.defer();
        var confirmPopup = $ionicPopup.confirm({
          cancelText: 'No',
          okText: 'Yes',
          okType: 'button-calm',
          title: title,
          template: message
        });
        confirmPopup.then(function(res) {
          return d.resolve(res);
        });
        return d.promise;
      }
    };
  })
  .factory("DateHelper", function($filter) {
    return {
      _default: "yyyy-mm-dd",
      formats: {
        "dd-mm-yyyy": "dd-MM-yyyy",
        "dd/mm/yyyy": "dd/MM/yyyy",
        "yyyy/mm/dd": "yyyy/MM/dd",
        "yyyy-mm-dd": "yyyy-MM-dd",
        "yyyy-dd-mm": "yyyy-dd-MM"
      },
      formatDate: function(date, format) {
        if (!format) {
          format = this._default;
        }
        var out = $filter('date')(date, this.formats[format]);
        return out;
      },
      toDate: function(date) {
        date = date.substring(0, 10);
        return date;
      },
      datediff: function(startDate, endDate) {
        var one_day = 1000 * 60 * 60 * 24;
        var sTime = new Date(startDate).getTime();
        var eTime = new Date(endDate).getTime();
        var out = Math.ceil((eTime - sTime)) / one_day;
        return out;
      },
      toDay: function(format) {
        return this.formatDate(new Date(), format);
      },
      addDay: function(date, days, format) {
        var result = new Date(date).getTime() + ((1000 * 60 * 60 * 24) * days);
        var out = this.formatDate(result, format);
        return out;
      }
    };
  });