'use strict';

angular.module('MobileApp.services')
  .service('Adapter', function Adapter($http, WTSURL, Schema, Authenticate, $cordovaDevice) {
    // AngularJS will instantiate a singleton by calling "new" on this function    
    function removeFields(obj, fields) {
      angular.forEach(fields, function(field) {
        var isChildField = _.contains(field, '.');
        if (isChildField) {
          var fieldToken = field.split('.');
          var childField = fieldToken[0];
          var fieldProperty = fieldToken[1];
          if (_.isArray(obj[childField])) {
            angular.forEach(obj[childField], function(item) {
              delete item[fieldProperty];
            });
          } else {
            delete obj[childField][fieldProperty];
          };
        } else {
          delete obj[field];
        };
      });
      return obj;
    }

    function getDeviceId() {
      try {
        var device = $cordovaDevice.getUUID();
        return device;
      } catch(err) {
        return "Olo-Test";
      }
    }

    return {
      deviceId: getDeviceId(),
      rootUrl: WTSURL,
      query: function(klass, properties, filter, oids) {
        var data = {};
        var self = this;
        
        data.command = 'GetClassObjects';
        data.parameters = {};
        data.parameters.class = Schema[klass].name;        
        data.parameters.filter = filter || {};
        data.parameters.token = Authenticate.currentUser.token;
        data.parameters.properties = properties || Schema[klass].properties || {};
        data.device = self.deviceId;
        if (oids) {
          data.parameters.oids = oids
        }
        return $http.post(self.rootUrl, data);
      },
      create: function(klass, params, fieldsToIgnore) {
        var data = {};
        var self = this;
        if (fieldsToIgnore) {
          params = removeFields(params, fieldsToIgnore);
        }
        data.command = 'UpdateObject';
        data.parameters = {};
        data.parameters.class = Schema[klass].name;
        data.parameters.userid = Authenticate.currentUser.name;
        data.parameters.token = Authenticate.currentUser.token;
        data.parameters = angular.extend(data.parameters, params);
        data.device = self.deviceId;
        return $http.post(self.rootUrl, data);
      },
      update: function(klass, params, fieldsToIgnore) {
        return this.create(klass, params, fieldsToIgnore);
      },
      destroy: function(klass, oid) {
        var data = {};
        var self = this;
        data.command = 'DeleteObject';
        data.parameters.class = Schema[klass].name;
        data.parameters.token = Authenticate.currentUser.token;
        data.parameters = {
          oid: oid
        };
        data.device = self.deviceId;
        return $http.post(self.rootUrl, data);
      }
    };
  });