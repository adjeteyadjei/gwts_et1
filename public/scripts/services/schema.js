'use strict';

angular.module('MobileApp.services', [])
  .service('Schema', function Schema() {
    // AngularJS will instantiate a singleton by calling "new" on this function
    return {
      //STATIC CLASSES
      forest: {
        'name': 'SWforest',
        'properties': {
          'code': '',
          'name': '',
          'type': ''
        }
      },
      location: {
        'name': 'GWlocation',
        'properties': {
          'location': '',
          'isMill': '',
          'rSWoffice': {
            'type': '',
            'officeType': '',
            'rSWdistrict': '',
            'rSWregion': ''
          },
          'rSWcountry': '',
          'rSWdistrict': '',
          'rSWlocationTown': '',
          'townCity': ''
        }
      },
      contractor: {
        'name': 'GWcontractor',
        'properties': {
          'code': '',
          'markRegistered': '',
          'name': ''
        }
      },
      species: {
        'name': 'GWspecies',
        'properties': {
          'code': '',
          'tradeName': '',
          'localName': '',
          'plantationSpecies': '',
          'naturalAndPlantation': ''
        }
      },
      landowner: {
        'name': 'GWlandOwner',
        'properties': {
          'name': ''
        }
      },

      //SOURCE CERTIFICATE CLASSES
      compartment: {
        'rSWforestCertificate': '',
        'rSWstockSurvey': '',
        'rSWcompartment': '',
        'no': '',
        'status': '',
        'rSWcompartmentLandOwner': ''
      },
      certCompartment: {
        'name': 'GWforestCertificateCompartment',
        'properties': this.compartment
      },
      certNatOnReserve: {
        'name': 'GWcertNaturalOnReserve',
        'properties': {
          'registerCode': '',
          'type': '',
          'rSWforest': {
            'code': ''
          },
          'rSWcontractor': {
            'code': '',
            'name': ''
          },
          'rSWforestCertCompartmentOid': {
            'rSWforestCertificate': '',
            'rSWstockSurvey': '',
            'rSWcompartment': '',
            'no': '',
            'status': ''
          }
        }
      },
      certNatOffReserve: {
        'name': 'GWcertNaturalOffReserve',
        'properties': {
          'registerCode': '',
          'type': '',
          'rSWcontractor': {
            'code': '',
            'name': ''
          },
          'rSWforest': {
            'code': ''
          },
          'rSWforestCertCompartmentOid': '',
          'rSWnaturalOffReserve': ''
        }
      },
      certPlantOnReserve: {
        'name': 'GWcertPlantationOnReserve',
        'properties': {
          'rSWdistrict': {},
          'rSWregion': {},
          'registerCode': '',
          'type': '',
          'rSWcontractor': {
            'code': '',
            'name': ''
          },
          'rSWforest': {
            'code': ''
          },
          'rSWforestCertificateArea': {'area': ''},
          'rSWlocation': {'location': ''}
        }
      },
      certPlantOffReserve: {
        'name': 'GWcertPlantationOffReserve',
        'properties': {
          'registerCode': '',
          'type': '',
          'rSWcontractor': {
            'code': '',
            'name': ''
          },
          'rSWforest': {
            'code': ''
          },
          'rSWforestCertCompartment': ''
        }
      },
      permit_salvage_on_reserve: { //Not confirmed
        'name': 'GWsalvagePermitOnReserve',
        'properties': {
          'registerCode': '',
          'type': '',
          'rSWcontractor': {
            'code': '',
            'name': ''
          },
          'rSWforest': {
            'code': ''
          },
          'rSWdeliveryDocketLine': '',
          'rSWdeliverySalvagedLogs': '',
          'rSWtree': '',
          'rSWtreeInformationSalvage': '',
          'totalVolume': '',
          'rSWsalvagePermitCompartment': '',
          'rSWsalvagePermitLineLog': ''
        }
      },
      permit_salvage_off_reserve: { //Not confirmed
        'name': 'GWsalvagePermitOffReserve',
        'properties': {
          'registerCode': '',
          'type': '',
          'rSWcontractor': {
            'code': '',
            'name': ''
          },
          'rSWforest': {
            'code': ''
          },
          'rSWdeliveryDocketLine': '',
          'rSWdeliverySalvagedLogs': '',
          'rSWtree': '',
          'rSWtreeInformationSalvage': '',
          'totalVolume': '',
          'rSWnaturalOffReserve': '',
          'rSWsalvagePermitLineLogNot': ''
        }
      },
      import_certificate: { //Not confirmed
        'name': 'GWimportCertificate',
        'properties': {
          'registerCode': '',
          'dateArrival': '',
          'rSWcontractor': '',
          'rSWcountryOrigin': '',
          'rSWdeliveryDocketLine': '',
          'rSWdeliveryImportLogs': '',
          'rSWexportRequestLineWood': '',
          'rSWimportCertificateLineLog': '',
          'rSWimportCertificateLineWood': '',
          'rSWinspectionCertificateImport': '',
          'rSWlocationPort': '',
          'totalVolume': '',
          'woodVolume': '',
          'woodVolumeUsed': ''
        }
      },

      //STOCK SURVEY CLASSES
      stock_survey_on_reserve: {
        'name': 'GWstockSurveyOnReserve',
        'properties': {
          'rSWforestCertificate': {
            'registerCode': '',
            'rSWforest': {
              'code': ''
            }
          },
          'rSWcheckSurveyOid':'',
          'rSWforestCertCompartment': {
            'no': ''
          },
          'no': '',
          'rSWcontractor': {
            'name': '',
            'code': '',
            'markRegistered': ''
          },
          'utmEasting': '',
          'utmNorthing': '',
          'teamLeader': '',
          'dateCommenced': '',
          'locked': '',
          'suitableForHarvest': ''
        }
      },
      stock_survey_off_reserve: {
        'name': 'GWstockSurveyOffReserve',
        'properties': {
          'rSWforestCertificate': {
            'registerCode': '',
            'rSWforest': {
              'code': ''
            }
          },
          'no': '',
          'rSWcontractor': '',
          'utmEasting': '',
          'utmNorthing': '',
          'teamLeader': '',
          'dateCommenced': '',
          'locked': ''
        }
      },
      stock_enumeration_off_reserve: {
        'name': 'GWstockEnumerationOffReserve',
        'properties': {
          'rSWforestCertificate': {
            'registerCode': '',
            'rSWforest': {
              'code': ''
            }
          },
          'rSWcontractor': '',
          'no': '',
          'totalUnits': '',
          'supervisor': ''
        }
      },
      stripline: {
        'name': 'GWstripLine',
        'properties': {
          'no': '',
          'lineNo': '',
          'direction': '',
          'length': '',
          'offset': '',
          'rSWstockSurvey': ''
        }
      },
      stripline_item: {
        'name': 'GWstripLineItem',
        'properties': {
          'rSWspecies': {
            'code': '',
            'tradeName': ''
          },
          'rSWstripLine': '',
          'type': '',
          'stockNo': '',
          'accessIssues': '',
          'canoeTree': '',
          'damaged': '',
          'seedTree': '',
          'dbh': '',
          'offset': '',
          'conditionScore': '',
          'metresAlong': ''
        }
      },
      check_survey_on_reserve: {
        'name': 'GWcheckSurvey',
        'properties': {
          'no': '',
          'rSWstockSurveyOid':{
            'no': ''
          },
          'compartment': {
            'no': ''
          },
          'rSWcontractor': {
            'name': '',
            'code': '',
            'markRegistered': ''
          },
          'utmEasting': '',
          'utmNorthing': '',
          'baseLineLength': '',
          'supervisor': '',
          'checkComplete': '',
          'teamLeader': ''
        }
      },
      check_stripline: {
        'name': 'GWstripLineCheck',
        'properties': {
          'no': '',
          'lineNo': '',
          'direction': '',
          'length': '',
          'offset': '',
          'rSWcheckSurvey': '',
          'rSWstripLine': ''
        }
      },
      check_stripline_item: {
        'name': 'GWstripLineCheckItem',
        'properties': {
          'rSWspecies': {
            'code': '',
            'tradeName': ''
          },
          'rSWstripLineItem':'',
          'rSWstripLineCheck':'',
          'type': '',
          'suffix':'',
          'stockNo': '',
          'accessIssues': '',
          'canoeTree': '',
          'damaged': '',
          'seedTree': '',
          'dbh': '',
          'offset': '',
          'conditionScore': '',
          'metresAlong': ''
        }
      },

      //HARVESTING CLASSES
      tif: {
        'name': 'GWtreeInformation',
        'properties': {
          'createdBy': '',
          'no': '',
          'rSWforestCertificate': {
            'registerCode': ''
          },
          'rSWforestCertCompartment': {
            'no': ''
          },
          'rSWcontractor': {
            'code': '',
            'name': ''
          },
          'supervisor': '',
          'date': '',
          'totalVolume': '',
          'rSWforest': '',
          'rSWcompartment': '',
          'rSWlandOwner': '',
          'notes': '',
          'rSWtreeInformationLine': {
            'stockNo': '',
            'contractorNo': '',
            'db1': '',
            'db2': '',
            'dt1': '',
            'dt2': '',
            'length': '',
            'volume': '',
            'rSWspecies': '',
            'rSWtreeInformation': ''
          }
        },
        lif: {
          'name': 'GWlogInformation',
          'properties': {
            'createdBy': '',
            'no': '',
            'rSWforestCertificate': '',
            'rSWforestCertCompartment': '',
            'rSWcontractor': '',
            'supervisor': '',
            'date': '',
            'totalVolume': '',
            'fellingAuthorityReference': '',
            'rSWcompartment': '',
            'rSWlogInformationLine': ''
          }
        },
        tree: {
          'name': 'GWtreeInformationLine',
          'properties': {
            'stockNo': '',
            'contractorNo': '',
            'rSWspecies': '',
            'db1': '',
            'db2': '',
            'dt1': '',
            'dt2': '',
            'length': '',
            'rSWtreeInformation': ''
          }
        },
        log: {
          'name': 'GWlogInformationLine',
          'properties': {
            'stockNo': '',
            'contractorNo': '',
            'rSWspecies': '',
            'db1': '',
            'db2': '',
            'dt1': '',
            'dt2': '',
            'dmp': '',
            'length': '',
            'stumpBarcode': '',
            'logNo': '',
            'logBarcode': '',
            'tIFNo': '',
            'rSWlogInformation': '',
            'rSWdeliveryDocketLine': ''
          }
        },
        stif: {
          'name': 'GWtreeInformationSalvage',
          'properties': {
            'createdBy': '',
            'no': '',
            'rSWcontractor': '',
            'supervisor': '',
            'date': '',
            'rSWsalvagePermit': '',
            'rSWtreeInformationSalvageLine': ''
          }
        }
      },
      stree: {
        'name': 'GWtreeInformationSalvageLine',
        'properties': {
          'contractorNo': '',
          'rSWspecies': '',
          'db1': '',
          'db2': '',
          'dt1': '',
          'dt2': '',
          'length': '',
          'rSWtreeInformationSalvage': ''
        }
      },
      ppc: { // Not confirmed
        'name': 'GWplantationProduction',
        'properties': {
          'createdBy': '',
          'no': '',
          'supervisor': '',
          'rSWforestCertificate': '',
          'rSWforestCertificateAreaOid':'',
          'rSWlocationOid': '',
          'rSWcontractor': '',
          'date': '',
          'notes':'',
          'rSWplantationProductionLine':''
        }
      },
      plog: {
        'name': 'GWplantationProductionLine',
        'properties': {
          'logNo': '',
          'contractorNo': '',
          'stockNo': '',
          'dmp': '',
          'db1': '',
          'db2': '',
          'dt1': '',
          'dt2': '',
          'length': '',
          'volume': '',
          'rSWplantationProduction': ''
        }
      },

      //TRANSPORTATION CLASSES
      lmcc: {
        'name': 'GWdeliveryFromNatural',
        'properties': {
          'createdBy': '',
          'no': '',
          'rSWforestCertificate': {
            'registerCode': ''
          },
          'rSWforestCertCompartment': {
            'no': ''
          },
          'rSWcontractor': {
            'name': ''
          },
          'supervisor': '',
          'date': '',
          'localityMark': '',
          'driver': '',
          'vehicle': '',
          'rSWdestination': {
            'location': ''
          },
          'totalVolume': '',
          'rSWdeliveryDocketLine': {
            'stockNo': '',
            'contractorNo': '',
            'rSWspecies': {
              'code': '',
              'tradeName': '',
              'localName': ''
            },
            'db1': '',
            'db2': '',
            'dt1': '',
            'dt2': '',
            'volume': '',
            'dmp': '',
            'length': '',
            'stumpBarcode': '',
            'logNo': '',
            'logBarcode': '',
            'tIFNo': ''
          },
          'rSWcheckPoint': '',
          'rSWcompartment': {
            'no': ''
          },
          'rSWforest': '',
          'signOffDate1': '',
          'signOffDate2': '',
          'signOffDate3': '',
          'signOffPerson1': '',
          'signOffPerson2': '',
          'signOffPerson3': ''
        }
      },
      plmcc: {
        'name': 'GWdeliveryFromPlantation',
        'properties': {
          'createdBy': '',
          'no': '',
          'rSWforestCertificate': '',
          'rSWcontractor': '',
          'supervisor': '',
          'date': '',
          'driver': '',
          'vehicle': '',
          'rSWdestination': '',
          'rSWdeliveryDocketLine': '',
          'rSWdeliverySummaryLine': '',
          'rSWstand': '',
          'rSWplantationProduction': '',
          'rSWcheckPoint': ''
        }
      },
      ilmcc: {
        'name': 'GWdeliveryImportLogs',
        'properties': {
          'createdBy': '',
          'no': '',
          'rSWimportCertificate': '',
          'rSWcontractor': '',
          'supervisor': '',
          'date': '',
          'driver': '',
          'vehicle': '',
          'rSWdestination': '',
          'rSWdeliveryDocketLine': '',
          'rSWcheckPoint': ''
        }
      },
      slmcc: {
        'name': 'GWdeliverySalvagedLogs',
        'properties': {
          'createdBy': '',
          'no': '',
          'rSWsalvagePermit': '',
          'rSWsalvagePermitCompartment': '',
          'rSWcontractor': '',
          'supervisor': '',
          'date': '',
          'driver': '',
          'vehicle': '',
          'rSWdestination': '',
          'rSWdeliveryDocketLine': '',
          'rSWcheckPoint': ''
        }
      },
      relmcc: {
        'name': 'GWremeasurementNatural',
        'properties': {
          'createdBy': '',
          'no': '',
          'date': '',
          'rSWcontractor': '',
          'supervisor': '',
          'notes': '',
          'rSWlocation': '',
          'rSWremeasurementLine': '',
          'rSWdeliveryFromNatural': '',
          'rSWcheckPoint': ''
        }
      },
      check_point_register: {
        'name': 'GWcheckPointRegister',
        'properties': {
          'createdBy': '',
          'no': '',
          'date': '',
          'checkedBy': '',
          'supervisor': '',
          'notes': '',
          'documentNo': '',
          'documentType': '',
          'rSWcheckPoint': '',
          'rSWdestination': '',
          'rSWtruck': '',
          'remarks': '',
          'vehicle': '',
        }
      },
      ltc: {
        'name': 'GWdeliveryFromLocation',
        'properties': {
          'createdBy': '',
          'no': '',
          'rSWsource': '',
          'supervisor': '',
          'date': '',
          'driver': '',
          'vehicle': '',
          'rSWdestination': '',
          'rSWdeliveryDocketLine': ''
        }
      },
      clmcc: { //Not Confirmed
        'name': '',
        'properties': {
          'createdBy': '',
          'no': '',
          'rSWrecoveryPermit': '',
          'rSWcontractor': '',
          'supervisor': '',
          'date': '',
          'driver': '',
          'vehicle': '',
          'rSWdestination': '',
          'rSWdeliveryDocketLine': '',
          'rSWcheckPoint': ''
        }
      }
    }

  });