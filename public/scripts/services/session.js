'use strict';

angular.module('MobileApp.services')
  .service('Session', function Session(Adapter, Schema, $q, Sync, Store, Certificates, Compartments, PlantationPermits) {
    var SessionModels = ['forest', 'location', 'contractor', 'species', 'landowner'];

    function loadStaticModel(model) {
      var d = $q.defer();
      var progress = {};
      var obj = new Store(model);
      d.notify('Checking ' + model + ' on device');
      if (obj.all().length < 1) {
        d.notify('Fetching ' + model);
        Adapter.query(model).then(function(response) {
          var result = response.data;
          if (result.status) {
            d.notify(model + ' received');
            obj.post(result.data.objects);
            if (obj.all().length > 0) {
              d.notify(model + ' saved on device');
              d.resolve(model);
            } else {
              d.notify('LOCAL ERROR : Could not save ' + model + ' data')
              d.reject(err);
            }
          } else {
            d.notify('SERVER ERROR : Could not load ' + model + ' from Server');
            d.reject('Server Error');
          }
        }, function(err) {
          d.reject('Error Connecting to server');
        });
      } else {
        d.resolve(model);
      }
      return d.promise;
    }

    function getPlantationModel(forestType) {
      return forestType == "Nat.On" ? "certPlantOnReserve" : "certPlantOffReserve";
    }

    function loadPlantationData(forest) {
      var model = getPlantationModel(forest.type); //Get certificate class to query
      var props = Schema[model].properties; //Represents certificate class properties
      var filter = {
        rSWforest: forest.oid
      };

      Adapter.query(model, props, filter).then(function(res) {
        if (res.status) {
          PlantationPermits.drop();
          PlantationPermits.post(res.data.data.objects);
        };
      });
    }

    function getCertModel(forestType) {
      switch (forestType) {
        case "Nat.On":
          return "certNatOnReserve";
        case "Nat.Off":
          return "certNatOffReserve";
          break;
        case "Plant.On":
          return "certPlantOnReserve";
          break;
        case "Plant.Off":
          return "certPlantOffReserve";
          break;
        default:
          return "certPlantOffReserve";
      }
    }
    return {
      current: {},
      init: function() {
        var d = $q.defer();
        var cnt = 0
        angular.forEach(SessionModels, function(model) {
          loadStaticModel(model).then(function(res) {
            cnt = cnt + 1;
            if (cnt == 5) {
              d.resolve(true);
            }
          }, function(err) {
            d.reject(err);
          }, function(note) {
            d.notify(note);
          });
        });

        return d.promise;
      },
      loadOperationalData: function(work) {
        var d = $q.defer();

        //Clear Certificates and Compartments
        Certificates.drop();
        Compartments.drop();

        var model = getCertModel(work.forest.type); //Get certificate class to query
        var props = Schema[model].properties; //Represents certificate class properties

        //Filter by forest
        var filter = {
          rSWforest: work.forest.oid
        };

        //Filter by compartment status [survey,havest,transport]
        props.rSWforestCertCompartmentOid.filter = {
          status: work.operation.code
        };

        //Query from certificates with filters
        Adapter.query(model, props, filter).then(function(res) {
          if (res.status) {

            var certs = []; //Hold Certificates Received From Request
            var cmpts = []; //Hold Certificate Compartments Received From Request

            //Refining Objects
            certs = res.data.data.objects.map(function(record) {
              cmpts.push.apply(cmpts, record.rSWforestCertCompartmentOid);
              delete record.rSWforestCertCompartmentOid;
              return record;
            });

            //Save certificates
            Certificates.drop();
            Certificates.post(certs);

            //Compartments
            Compartments.drop();
            Compartments.post(cmpts);

            //Load Plantation Data
            loadPlantationData(work.forest);

            //Message: [ForestName] [OpertionName] data loaded successfully.
            var res = angular.lowercase(work.forest.name) + " " + angular.lowercase(work.operation.name) + " data loaded successfully.";
            return d.resolve(res);
          };
          $ionicLoading.hide();
        }, function(err) {
          return d.reject("Error connecting to server"); //err.data
        });
        return d.promise;
      },
      getSurveyData: function() {

      }
    };
  });