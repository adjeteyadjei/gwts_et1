'use strict';
// Ionic Starter App, v0.9.20

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('MobileApp', ['ionic', 'config', 'angularLocalStorage', 'MobileApp.controllers', 'MobileApp.services', 'ngCordova', 'ui.select2', 'ui.bootstrap', 'ionicSelect'])
//.value('WTSURL','http://www.ssi.co.nz/testgwtsws?GWTSWebServices&serviceName=GWwebService&op=GWwebService::actionRequest')
//.value('WTSURL','http://41.66.197.203:81/liverestfulservice/jadehttp.dll?RestfulJsonServiceGW')
.value('WTSURL','http://41.66.197.203/gwtsws/jadehttp.dll?GWTSWebServices&listName=WebServices&serviceName=GWwebService')
//.value('WTSURL','http://197.253.69.36:8080/api/mobile/execute')
//.value('WTSURL','http://41.66.197.205/api/mobile/execute')
//.value('WTSURL', 'api')
// .config(function(uiSelectConfig) {
//   uiSelectConfig.theme = 'select2';
// })
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    navigator.splashscreen.hide()
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })
      .state('app.login', {
        url: '/login',
        views: {
          'menuContent': {
            templateUrl: 'templates/login.html',
            controller: 'AppCtrl'
          }
        }
      })
      .state('app.welcome', {
        url: '/welcome',
        views: {
          'menuContent': {
            templateUrl: 'templates/welcome.html',
            controller: 'WelcomeCtrl'
          }
        }
      })
      .state('app.search', {
        url: '/search',
        views: {
          'menuContent': {
            templateUrl: 'templates/search.html',
            controller: 'SearchCtrl'
          }
        }
      })
      .state('app.harvest', {
        url: '/harvest',
        views: {
          'menuContent': {
            templateUrl: 'templates/harvest.html',
            controller: 'HarvestCtrl'
          }
        }
      })
      .state('app.transport', {
        url: '/transport',
        views: {
          'menuContent': {
            templateUrl: 'templates/transport.html',
            controller: 'TransportCtrl'
          }
        }
      })
      .state('app.checkpoint', {
        url: '/checkpoint',
        views: {
          'menuContent': {
            templateUrl: 'templates/checkpoint/home.html'
          }
        }
      })
      .state('app.checkcertificate', {
        url: '/checkcertificate',
        views: {
          'menuContent': {
            templateUrl: 'templates/checkpoint/check.html',
            controller: 'CheckpointCtrl'
          }
        }
      })
      .state('app.register', {
        url: '/register',
        views: {
          'menuContent': {
            templateUrl: 'templates/checkpoint/register.html'
          }
        }
      })

    .state('app.docket', {
      url: '/docket',
      views: {
        'menuContent': {
          templateUrl: 'templates/checkpoint/docket.html',
          controller: 'DocketCtrl'
        }
      }
    })
      .state('app.rlmcc', {
        url: '/rlmcc',
        views: {
          'menuContent': {
            templateUrl: 'templates/checkpoint/rlmcc.html',
            controller: 'RlmccCtrl'
          }
        }
      })
      .state('app.sync', {
        url: '/sync',
        views: {
          'menuContent': {
            templateUrl: 'templates/sync.html',
            controller: 'SyncCtrl'
          }
        }
      })
      .state('app.settings', {
        url: '/settings',
        views: {
          'menuContent': {
            templateUrl: 'templates/settings/Session.html'
          }
        }
      })
      .state('app.tifs', {
        url: '/tifs',
        views: {
          'menuContent': {
            templateUrl: 'templates/tif/list.html',
            controller: 'TifsCtrl'
          }
        }
      })
      .state('app.tif', {
        url: '/tifs/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/tif/show.html',
            controller: 'TifCtrl'
          }
        }
      })
      .state('app.edittif', {
        url: '/tif/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/tif/tif.html',
            controller: 'TifCtrl'
          }
        }
      })
      .state('app.tree', {
        url: '/tree/:tif/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/tif/tree.html',
            controller: 'TreeCtrl'
          }
        }
      })
      .state('app.ppcs', {
        url: '/ppcs',
        views: {
          'menuContent': {
            templateUrl: 'templates/ppc/list.html',
            controller: 'PpcsCtrl'
          }
        }
      })
      .state('app.ppc', {
        url: '/ppcs/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/ppc/show.html',
            controller: 'PpcCtrl'
          }
        }
      })
      .state('app.editppc', {
        url: '/ppc/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/ppc/ppc.html',
            controller: 'PpcCtrl'
          }
        }
      })
      .state('app.plog', {
        url: '/plog/:ppc/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/ppc/plog.html',
            controller: 'PlogCtrl'
          }
        }
      })

    .state('app.log', {
      url: '/log/:lmcc/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/lmcc/log.html',
          controller: 'LogCtrl'
        }
      }
    })
      .state('app.lmccs', {
        url: '/lmccs',
        views: {
          'menuContent': {
            templateUrl: 'templates/lmcc/list.html',
            controller: 'LmccsCtrl'
          }
        }
      })
      .state('app.lmcc', {
        url: '/lmccs/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/lmcc/show.html',
            controller: 'LmccCtrl'
          }
        }
      })
      .state('app.editlmcc', {
        url: '/lmcc/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/lmcc/lmcc.html',
            controller: 'LmccCtrl'
          }
        }
      })
      .state('app.plantlog', {
        url: '/plantlog/:plmcc/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/plmcc/log.html',
            controller: 'PlantLogCtrl'
          }
        }
      })
      .state('app.plantentry', {
        url: '/plantentry/:plmcc/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/plmcc/entry.html',
            controller: 'PlantEntryCtrl'
          }
        }
      })
      .state('app.plmccs', {
        url: '/plmccs',
        views: {
          'menuContent': {
            templateUrl: 'templates/plmcc/list.html',
            controller: 'PlmccsCtrl'
          }
        }
      })
      .state('app.plmcc', {
        url: '/plmccs/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/plmcc/show.html',
            controller: 'PlmccCtrl'
          }
        }
      })
      .state('app.editplmcc', {
        url: '/plmcc/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/plmcc/plmcc.html',
            controller: 'PlmccCtrl'
          }
        }
      })
      .state('app.lifs', {
        url: '/lifs',
        views: {
          'menuContent': {
            templateUrl: 'templates/lif/list.html',
            controller: 'LifsCtrl'
          }
        }
      })
      .state('app.lif', {
        url: '/lifs/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/lif/show.html',
            controller: 'LifCtrl'
          }
        }
      })
      .state('app.editlif', {
        url: '/lif/:id',
        views: {
          'menuContent': {
            templateUrl: 'templates/lif/lif.html',
            controller: 'LifCtrl'
          }
        }
      })
    //Log Start
    .state('app.showlif', {
      url: '/lifs/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/lif/show.html',
          controller: 'LifCtrl'
        }
      }
    })
    //Log Ends
    .state('app.survey', {
      url: '/survey',
      views: {
        'menuContent': {
          templateUrl: 'templates/survey/list.html',
          controller: 'surveyCtrl'
        }
      }
    })
    .state('app.striplines', {
      url: '/striplines/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/striplines/list.html',
          controller: 'striplinesCtrl'
        }
      }
    })
    .state('app.newStriplineForm', {
      url: '/stripline/{type}/:surveyId',
      views: {
        'menuContent': {
          templateUrl: 'templates/striplines/form.html',
          controller: 'striplinesCtrl'
        }
      }
    })
    .state('app.editStriplineform', {
      url: '/editstripline/{type}/:surveyId/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/striplines/form.html',
          controller: 'striplinesCtrl'
        }
      }
    })
    .state('app.showStriplineform', {
      url: '/showstripline/{type}/:surveyId/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/striplines/show.html',
          controller: 'striplinesCtrl'
        }
      }
    })
    .state('app.striplineItems', {
      url: '/striplineItems/:striplineId',
      views: {
        'menuContent': {
          templateUrl: 'templates/striplineItems/list.html',
          controller: 'striplineItemsCtrl'
        }
      }
    })
    .state('app.newStriplineItemForm', {
      url: '/newstriplineitem/{type}/:striplineId',
      views: {
        'menuContent': {
          templateUrl: 'templates/striplineItems/form.html',
          controller: 'striplineItemsCtrl'
        }
      }
    })
    .state('app.editStriplineItem', {
      url: '/editstriplineitem/{type}/:striplineId/:itemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/striplineItems/form.html',
          controller: 'striplineItemsCtrl'
        }
      }
    })
    .state('app.showStriplineItem', {
      url: '/showstriplineitem/{type}/:striplineId/:itemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/striplineItems/show.html',
          controller: 'striplineItemsCtrl'
        }
      }
    })
    .state('app.check', {
      url: '/check',
      views: {
        'menuContent': {
          templateUrl: 'templates/surveycheck/list.html',
          controller: 'checkCtrl'
        }
      }
    })
    .state('app.striplinescheck', {
      url: '/striplinescheck/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/striplinescheck/list.html',
          controller: 'striplinesCheckCtrl'
        }
      }
    })
    .state('app.striplineItemscheck', {
      url: '/striplineItemscheck/:striplineId',
      views: {
        'menuContent': {
          templateUrl: 'templates/striplineItemscheck/list.html',
          controller: 'striplineItemsCheckCtrl'
        }
      }
    })
    .state('app.newStriplineItemFormcheck', {
      url: '/newstriplineitemcheck/{type}/:striplineId',
      views: {
        'menuContent': {
          templateUrl: 'templates/striplineItemscheck/form.html',
          controller: 'striplineItemsCheckCtrl'
        }
      }
    })
    .state('app.editStriplineItemcheck', {
      url: '/editstriplineitemcheck/{type}/:striplineId/:itemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/striplineItemscheck/form.html',
          controller: 'striplineItemsCheckCtrl'
        }
      }
    })
    .state('app.showStriplineItemcheck', {
      url: '/showstriplineitemcheck/{type}/:striplineId/:itemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/striplineItemscheck/show.html',
          controller: 'striplineItemsCheckCtrl'
        }
      }
    });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/welcome');
  });