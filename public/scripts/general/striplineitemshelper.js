$("#treeSelection").change(function(val,e){ switch_views(); });
$("#dbh_large").on("click", dbh_large());

// Onload 
$(document).ready(function(){
  switch_views();
  dbh_large();
})

function dbh_large(){
  if($("#dbh_large").prop("checked")){
    $("#dbh").val("150");
  }else{
    $("#dbh").val("");
  }
}

function switch_views(){
  var selected = $("#treeSelection").val();
  switch(selected) {
    case "Tree":
    $("#condition_details").hide();
    $("#treedetails").show();
    $('#metresAlong' ).parsley( 'removeConstraint', 'multiple' );
    $('#conditionScore' ).parsley( 'removeConstraint', 'required');
    add_tree_details_constraints();
    break;
    case "Condition Score":
    $("#condition_details").show();
    $("#treedetails").hide();
    $('#metresAlong' ).parsley( 'addConstraint', {multiple: 60 } );
    $('#conditionScore' ).parsley( 'addConstraint', {required: true } );
    rm_tree_details_constraints();
    break;
    default:
    $("#condition_details").hide();
    $("#treedetails").hide();
    $('#metresAlong' ).parsley( 'removeConstraint', 'multiple' );
    $('#conditionScore' ).parsley( 'removeConstraint', 'required');
    rm_tree_details_constraints();
    break;
  }
}

function rm_tree_details_constraints(){
  $('#offset' ).parsley( 'removeConstraint', 'required');
  $('#stockNo' ).parsley( 'removeConstraint', 'required');
  $('#dbh' ).parsley( 'removeConstraint', 'required');
}

function add_tree_details_constraints(){
  $('#offset' ).parsley( 'addConstraint', {required: true } );
  $('#stockNo' ).parsley( 'addConstraint', {required: true } );
  $('#dbh' ).parsley( 'addConstraint', {required: true } );
}

$('#the-form').parsley({
  validators: {
    multiple: function ( val, multiple ) { return val % multiple === 0;}
  },
  messages: { multiple: "This value should be a multiple of %s" }
});

$(document).ready(function() {
  switch_views();
  $('input.check').prettyCheckable();
  $("#fsubmit").click(function(){
    $.ajax({
      url: "<%= url_for :action => :init %>",
      type:"POST",
      data: {
        direction : $('#operation').val(),
        forest_code : $('#forest_code').val(),
        do_sync: $("#do_sync").prop("checked") ? "yes":"no"
      }
    }).done(function ( data ) {
      $("#help").addClass("alert alert-danger");
      $("#help").html(data.msg);
    });
  });
});