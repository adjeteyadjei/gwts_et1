'use strict';
angular.module('MobileApp.controllers', [])
  .controller('AppCtrl', function($scope, Store, $ionicModal, $timeout, $ionicPopup, $state, $ionicLoading, $ionicPlatform, Authenticate, db, Session, Model, storage, MSG, $cordovaNetwork) {
    //Access Local Storage
    $scope.version = "GWTS Mobile v3.1.2D";
    window.ss = storage;
    window.Store = Store;
    // Form data for the login modal
    $scope.loginStatus = Authenticate.secure();

    setInterval(function() {
      if (navigator.connection) {
        var networkState = navigator.connection.type;
        $scope.isOnline = (networkState == 'none') ? false : true
        console.log($scope.isOnline);
      };
    }, 5000);

    


    if ($scope.loginStatus) {
      $scope.currentUser = Authenticate.currentUser.name;
    }
    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
      if (!Authenticate.secure()) $scope.login();
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
      $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
      $scope.modal.show();
    };

    // Open the login modal
    $scope.logout = function() {
      MSG.confirm("User Log out", "Are you sure you want to log out").then(function(res) {
        if (res) {
          Authenticate.logOut();
          $scope.login();
        }
      });
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function(loginData) {
      $ionicLoading.show({
        template: 'Connecting to server.......'
      });
      Authenticate.userLogin(loginData).then(function(response) {
        var responseData = response.data;
        console.log(responseData);
        if (responseData.status) {
          Authenticate.setCurrentUser({
            name: loginData.username,
            token: responseData.data.token,
            tokenStatus: responseData.tokenStatus
          });
          $scope.closeLogin();
          $ionicLoading.hide();
          $scope.loginStatus = Authenticate.secure();
          $scope.currentUser = Authenticate.currentUser.name;
          $state.go('app.welcome');
        } else {
          $ionicLoading.hide();
          $ionicPopup.alert({
            title: 'Could not Login',
            template: responseData.errorMessage
          });
        }
      }, function(err) {
        $ionicLoading.show({
          template: 'Could not Connect to server',
          duration: 3000
        });
      });
    };
  })

.controller('SearchCtrl', function($scope, Model) {
  var model = new Model('compartment');
  window.mmm = model;

  function start(argument) {
    console.log('starting');
    model.all().then(function(res) {
      console.log('compartment');
      console.log(res);
      $scope.items = res;
    }).catch(function(err) {
      console.log(err);
    });
    //console.log('end');
  }
  start();
});
