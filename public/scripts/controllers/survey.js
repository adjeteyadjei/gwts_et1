'use strict'
angular.module('MobileApp.controllers')
	.controller('surveyCtrl', function($scope, StockSurvey, Striplines, StriplineItems, CheckSurvey, CheckStripLines, CheckStripLineItems, $ionicLoading, MSG, AppConfig, Adapter, $q, db, Schema) {
		window.msg = MSG;
		window.AppConfig = AppConfig;
		window.survey = StockSurvey;
		$scope.surveys = [];

		function loadSurveysFromDevice() {
			$scope.surveys = StockSurvey.all();
		}

		$scope.reloadSurvey = function() {
			StockSurvey.drop();
			fetchSurvey().then(
				function(res) {
					console.log("res");
					if (res) {
						console.log("Stock Survey Reloaded");
						loadSurveysFromDevice();
						$ionicLoading.hide();
					}
				},
				function(err) {
					$ionicLoading.show({
						template: err,
						duration: 3000
					});
				},
				function(note) {
					$ionicLoading.show({
						template: note
					});
				}
			);
		}

		function fetchSurvey() {
			var d = $q.defer();
			//window.surveys = stockSurvey;
			var surveys = StockSurvey.all();
			var change = false
			if (surveys.length > 0) {
				if (surveys[0].rSWforestCertificate.rSWforest.code != AppConfig.getForestCode()) change = true
			}

			if ((surveys.length < 1) || change) {
				$ionicLoading.show({
					template: 'Requesting active surveys from server...'
				});
				var model = 'stock_survey_on_reserve';
				var props = Schema[model].properties; //Represents certificate class properties

				//Filter out only compartments that are not suitable for harvest
				var filter = {
					completedTieLineSurvey: true,
					completedSurvey: false,
					suitableForHarvest: false, //Ensure its not suitable for harvest
					"rSWforestCertificate.rSWforest": AppConfig.getForestOid()
				};

				//Add stripline and stripline items to properties to return
				props['rSWstripLine'] = Schema['stripline'].properties;
				props['rSWstripLine']['rSWstripLineItem'] = Schema['stripline_item'].properties;


				Adapter.query(model, props, filter).then(function(res) { //make another request for this one but this time bring down the striplines
					var result = res.data
					if (result.status) {
						$ionicLoading.show({
							template: 'Processing striplines........'
						});
						var stocksurveysData = result.data.objects;
						var stocksurveys = [];
						var striplineItems = [];
						var striplines = [];
						Striplines.drop();
						angular.forEach(stocksurveysData, function(survey) {
							angular.forEach(survey.rSWstripLineOid, function(stripline) {
								stripline.rSWstockSurvey = stripline.rSWstockSurvey.oid;
								stripline.isDirty = false;

								//Copy all Stripline Items on this stripline
								angular.forEach(stripline.rSWstripLineItemOid, function(item) {
									item.rSWstripLine = item.rSWstripLine.oid;
									item.isDirty = false;
									if (item.rSWspecies != undefined) {
										item.rSWspecies = item.rSWspecies.oid;
									}
									this.push(item);
								}, striplineItems);

								//Remove stripline Items from Stripline::Very important
								delete stripline.rSWstripLineItemOid;

								//Save Stripline to list
								this.push(stripline);
							}, striplines);
							delete survey.rSWstripLineOid;
							this.push(survey)
						}, stocksurveys);

						$ionicLoading.show({
							template: 'Finalising stock surveys........'
						});
						StockSurvey.drop();
						StockSurvey.post(stocksurveys);
						$ionicLoading.show({
							template: 'Stock surveys saved on device'
						});

						$ionicLoading.show({
							template: 'Finalising stock striplines........'
						});
						Striplines.drop();
						Striplines.post(striplines);
						$ionicLoading.show({
							template: 'Striplines saved on device'
						});

						$ionicLoading.show({
							template: 'Finalising stripline items........'
						});
						StriplineItems.drop();

						StriplineItems.post(striplineItems);
						$ionicLoading.show({
							template: 'stripline items saved on device'
						});
						d.resolve(true);

					} else {
						$ionicLoading.show({
							template: result.errorMessage,
							duration: 3000
						});
						d.reject();
					}
				}, function(err) {
					$ionicLoading.show({
						template: 'Error connecting to the server',
						duration: 3000
					});
					d.reject()
				}, function(err) {
					$ionicLoading.show({
						template: 'Error connecting to the server',
						duration: 3000
					});
					d.reject()
				});
			} else {
				d.resolve(true);
			}
			return d.promise;
		}

		fetchSurvey().then(
			function(res) {
				if (res) {
					loadSurveysFromDevice();
					$ionicLoading.hide();
				}
			});
	});