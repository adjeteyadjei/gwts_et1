'use strict';

/**
 * @ngdoc function
 * @name MobileApp.controllers.controller:CheckpointCtrl
 * @description
 * # CheckpointCtrl
 * Controller of the MobileApp.controllers
 */
angular.module('MobileApp.controllers')
	.controller('CheckpointCtrl', function($scope, $q, Schema, AppConfig, MSG, $ionicLoading, $state, Adapter) {
		$scope.docTypes = [{
			name: 'LMCC',
			code: 'lmcc'
		}, {
			name: 'Plantation LMCC',
			code: 'plmcc'
		}, {
			name: 'Log Transfer Certificate',
			code: 'ltc',
		}, {
			name: 'Import LMCC',
			code: 'ilmcc'
		}, {
			name: 'Recovery LMCC',
			code: 'relmcc'
		}, {
			name: 'Salvage LMCC',
			code: 'slmcc'
		}];

		function liveSearch(docket) {
			var d = $q.defer();
			var model = docket.docType.code;
			var props = Schema[model].properties;
			var filter = {
				no: docket.referenceNumber
			};

			Adapter.query(docket.docType.code, props, filter).then(function(res) {
				return d.resolve(res);
			}).catch(function(error) {
				d.reject(error);
			});
			return d.promise;
		}

		$scope.search = function(docket) {
			$ionicLoading.show({
				template: 'Searching...'
			});

			liveSearch(docket).then(function(res) {
				$ionicLoading.hide();
				if (res.data.status) {
					var data = res.data.data.objects;
					if (data.length > 0) {
						var dock = data[0];
						dock.dockType = docket.docType.code;
						AppConfig.dock(data[0]);
						$state.go('app.docket');
					} else {
						MSG.alert("No " + docket.docType.code + " found with reference number " + docket.referenceNumber);
					};
				} else {
					MSG.error(res.data.message);
				}
			}).catch(function(err) {
				MSG.error("Unable to process request.");
			})
		};


		function open(dock) {
			AppConfig.dock(dock)
			$state.go('docket');
		}
	})

.controller('DocketCtrl', function($scope, $state, $ionicLoading, AppConfig, MSG, Authenticate,Sync, DateHelper) {

	$scope.confirm = function() {
		MSG.confirm("Check Point", "Are you sure you want to confirm this certificate?").then(function(confirm) {
			if (confirm) {
				// Add signOffPeroson3 to the docket
				$scope.lmcc.signOffPeroson3 = Authenticate.currentUser.name;
				$scope.lmcc.signOffDate3 = DateHelper.toDay("yyyy-dd-mm");

				// Push updated docket back to server
				var fieldstoIgnore = ['dockType', 'rSWremeasurementLine.db',
					'rSWremeasurementLine.dt', 'rSWremeasurementLine.volume'
				];
				$ionicLoading.show({
					template: 'Sending confirmed certificate to server'
				});
				Sync.push($scope.lmcc.dockType, $scope.lmcc, fieldstoIgnore).then(function(res) {
					$ionicLoading.hide();
					if (res.status) {
						if (res.data.status) {
							MSG.success("Certificate Sent.");
							AppConfig.dock(null);
							$state.go('app.checkcertificate')
						} else {
							MSG.error(res.data.errorMessage);
						}
					}
				});
			};
		});
	};

	$scope.mismatch = function() {
		MSG.confirm("LMCC Mismatch", "Do you want to create a remeasurement certificate for this LMCC?").then(function(confirm) {
			if (confirm) {
				$state.go('app.rlmcc');
			};
		})
	};

	function start() {
		$scope.lmcc = AppConfig.dock();
		if (!$scope.lmcc) {
			$state.go('app.checkcertificate')
		};
	}

	start();

})

.controller('RlmccCtrl', function($scope, $state, $filter, AppConfig, MSG, $ionicModal , Authenticate,Species, DateHelper) {

	//Defining log model form
	$ionicModal.fromTemplateUrl('templates/lmcc/log.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.logForm = modal;
	});

	$scope.closeLogForm = function() {
		$scope.logForm.hide();
	};

	$scope.editLog = false;
	var logIndex = "";
	$scope.showLogForm = function(log, i) {
		if (log) {
			$scope.log = log;
			logIndex = i;
			$scope.editLog = true;
			$scope.title = "Edit Log";
		} else {
			$scope.log = {
				db1: 0,
				db2: 0,
				dt1: 0,
				dt2: 0,
				volume: 0
			};
			$scope.title = "Add Log"
		};
		$scope.logForm.show();
	};

	$scope.addLog = function(log) {
		if (checkDuplicate(log)) {
			MSG.error("Log No " + log.logNo + " already entered for TIF " + log.tIFNo);
			return;
		};
		log.lineNo = new Date().getTime();
		log.rSWspecies = log.specie.oid;
		$scope.rlmcc.rSWremeasurementLine.push(log);
		$scope.rlmcc.totalVolume = calculateTotalVolume();
		//Lmccs.save($scope.lmcc);
		$scope.closeLogForm();
	};

	function checkDuplicate(log) {
		var filter = {
			logNo: log.logNo,
			stockNo: log.stockNo
		};
		var obj = _.findWhere($scope.rlmcc.rSWremeasurementLine, filter);
		return (obj);
	}

	$scope.removeLog = function() {
		$scope.rlmcc.rSWremeasurementLine.splice(logIndex, 1);
		$scope.rlmcc.totalVolume = calculateTotalVolume();
		//Lmccs.save($scope.lmcc);
		$scope.closeLogForm();
	};

	$scope.$watch('log.db1', function() {
		doCalculation();
	});

	$scope.$watch('log.db2', function() {
		doCalculation();
	});

	$scope.$watch('log.dt1', function() {
		doCalculation();
	});

	$scope.$watch('log.dt2', function() {
		doCalculation();
	});

	$scope.$watch('log.length', function() {
		doCalculation();
	});

	$scope.$watch('log.branchVolume', function() {
		doCalculation();
	});

	function calculateTotalVolume() {
		var total = 0;
		if ($scope.rlmcc.rSWremeasurementLine) {
			angular.forEach($scope.rlmcc.rSWremeasurementLine, function(log, i) {
				total += parseFloat(log.volume);
			});
		};
		return $filter('number')(total, 3);
	}

	function doCalculation() {
		if ($scope.log) {
			//Calculate DB and DT
			$scope.log.db = (parseFloat($scope.log.db1) + parseFloat($scope.log.db2)) / 2;
			$scope.log.dt = (parseFloat($scope.log.dt1) + parseFloat($scope.log.dt2)) / 2;

			//Calculate Volumes
			var volume = (((Math.pow($scope.log.db, 2) + Math.pow($scope.log.dt, 2)) * 0.00007854 / 2).toFixed(2)) * parseFloat($scope.log.length);
			$scope.log.volume = $filter('number')(volume, 3);
		};
	}

	$scope.push = function() {
		MSG.confirm("RLMCC", "Are you sure you want to send this remeasurement to the server?").then(function(confirm) {
			if (confirm) {
				// Add signOffPeroson3 to the docket
				$scope.rlmcc.signOffPeroson3 = Authenticate.currentUser.name;
				$scope.rlmcc.signOffDate3 = DateHelper.toDay("yyyy-dd-mm");

				// Push updated docket back to server
				var fieldstoIgnore = ['dockType', 'rSWremeasurementLine.db',
					'rSWremeasurementLine.dt', 'rSWremeasurementLine.volume'
				];
				$ionicLoading.show({
					template: 'Sending remeasurement details to server'
				});
				Sync.push('relmcc', $scope.rlmcc, fieldstoIgnore).then(function(res) {
					$ionicLoading.hide();
					if (res.status) {
						if (res.data.status) {
							MSG.success("Remeasurement Certificate Sent.");
							AppConfig.dock(null);
							$state.go('app.checkcertificate')
						} else {
							MSG.error(res.data.errorMessage);
						}
					}
				});
			};
		});
	};

	function start() {
		$scope.species = Species.all();
		$scope.rlmcc = AppConfig.dock();
		if (!$scope.rlmcc) {
			$state.go('app.checkcertificate');
		}else{
			$scope.rlmcc.lmccNo = $scope.rlmcc.no;
			$scope.rlmcc.no = "Re-" + $scope.rlmcc.no;
		};
	}

	start();
});