'use strict';

angular.module('MobileApp')
    .controller('SettingsCtrl', function($scope, db) {

        function getButtonType(model) {
            return (db.isEmpty(model)) ? 'button-assertive' : 'button-positive';
        }

        $scope.SessionItems = [{
            name: 'Forests',
            model: 'forests',
            buttonIcon: 'ion-loop',
            buttonType: getButtonType('forest')
        }, {
            name: 'Species',
            model: 'species',
            buttonIcon: 'ion-loop',
            buttonType: getButtonType('species')
        }, {
            name: 'Contractors',
            model: 'contractors',
            buttonIcon: 'ion-loop',
            buttonType: getButtonType('contractor')
        }, {
            name: 'Land Owners',
            model: 'landowners',
            buttonIcon: 'ion-loop',
            buttonType: getButtonType('landowner')
        }];
    });