'use strict';

/**
 * @ngdoc function
 * @name MobileApp.controller:TifCtrl
 * @author olonka
 * # TifCtrl
 * Controllers for tif view
 * Controller of the MobileApp
 */
angular.module('MobileApp.controllers')
	.controller('TifsCtrl', function($scope, $q, Model, $ionicLoading, $ionicModal, MSG, Tifs, Sync) {
		$scope.tifs = [];

		function readTifs() {
			$scope.tifs = Tifs.all();
		}

		//Setting default list item height for collection repeat
		$scope.getItemHeight = function() {
			return 50;
		};

		$scope.delete = function(tif) {
			MSG.confirm('TIF', "Are you sure you want to delete this TIF?").then(function(confirm) {
				if (confirm) {
					var res = Tifs.delete(tif);
					res.success ? MSG.success(res.message) : MSG.error(res.message);
				};
			});
		};

		function getSyncables() {
			$scope.totalToSync = 0;
			$scope.tifs.forEach(function(tif) {
				if (tif.rSWtreeInformationLine.length > 0) {
					$scope.totalToSync++;
				}
			});
		}

		$ionicModal.fromTemplateUrl('templates/sync_report.html', {
			scope: $scope
		}).then(function(modal) {
			$scope.modal = modal;
		});

		$scope.close = function() {
			$scope.modal.hide();
		};

		var syncedTifs = [];
		var syncErrors = "";

		function syncData(argument) {
			var count = 0;
			syncErrors = "";
			var d = $q.defer();
			var fieldsToIgnore = ["_id", "_index", "generateId", "certificate",
				"compartment", "totalVolume", "rSWtreeInformationLine.boleVolume",
				"rSWtreeInformationLine.db", "rSWtreeInformationLine.dt", "rSWtreeInformationLine.volume",
				"rSWtreeInformationLine.totalVolume", "rSWtreeInformationLine.specie", "rSWtreeInformationLine.measurement",
				"rSWtreeInformationLine.lineNo", "rSWtreeInformationLine.branchVolume"
			];
			//Sync Report
			$scope.modal.show();
			var syncedItems = 0;
			$scope.report_title = "TIFs";
			$scope.success_report = "";
			$scope.failure_report = [];
			$scope.syncfinished = false;
			$scope.tifs.forEach(function(record) {
				if (record.rSWtreeInformationLine.length > 0) {
					Sync.push('tif', record, fieldsToIgnore).then(function(res) {
						count++;
						if (res.status) {
							if (res.data.status) {
								syncedItems++;
								syncedTifs.push(record);
								$scope.success_report = syncedItems + " out of " + $scope.totalToSync + " records sent to server";
							} else {
								var err = count + ": " + record.no + " -> " + res.data.errorMessage;
								syncErrors += err;
								$scope.failure_report.push(err);
							}
						}
						if (count == $scope.totalToSync) {
							return d.resolve(res);
							$scope.syncfinished = true;
						}
					});
				};

			});
			return d.promise;
		}

		$scope.sync = function() {
			MSG.confirm('Syncing TIF', "Are you sure you want to push TIFs to the server?").then(function(res) {
				if (res) {
					syncData().then(function(argument) {
						//Delete Synced TIFS
						syncedTifs.forEach(function(stif) {
							Tifs.delete(stif);
						});
						$scope.syncfinished = true;
						start();
					});
				};
			})
		};

		function start() {
			readTifs();
			getSyncables();
		}

		start();

	})

.controller('TifCtrl', function($scope, $ionicModal, $ionicLoading, $filter, AppConfig, DateHelper, $state, $stateParams, Model, MSG, Tifs, Species, Certificates, Compartments, Locations) {
	$scope.tif = {};

	function getTif(index) {
		$scope.tif = Tifs.findByIndex(index);
	}

	//Load all lookup data for operation
	function fetchData(argument) {
		$scope.certificates = Certificates.all();
		$scope.compartments = Compartments.all();
	}

	$scope.save = function(newTif) {
		var certificate = {};
		AppConfig.supervisor(newTif.supervisor);
		if (AppConfig.forest().type == "Nat.On") {
			//On Reserve TIF.
			certificate = Certificates.find({
				oid: newTif.compartment.rSWforestCertificate.oid
			})[0];

			//On Reserve TIF requires rSWforestCertCompartment and rSWcompartment
			newTif.rSWforestCertCompartment = newTif.compartment.oid;
			newTif.rSWcompartment = newTif.compartment.rSWcompartment.oid;

		} else {
			//Off Reserve TIF
			certificate = newTif.certificate;
		};

		//Error Handling if Certificate not found
		if (!certificate.oid) {
			MSG.error('Forest certificate of the selected compartment not found.' +
				' Please goto home view, select operation and sync data with server.');
			return;
		};


		//Setting common data [contractor,certificate,forest,line]
		newTif.certificate = certificate;
		newTif.rSWforestCertificate = certificate.oid;
		newTif.rSWforest = certificate.rSWforest.oid;
		newTif.rSWcontractor = certificate.rSWcontractor.oid;
		if (!newTif.rSWtreeInformationLine) {
			newTif.rSWtreeInformationLine = [];
		};


		//Reset Date Format
		newTif.date = DateHelper.formatDate(newTif.date, "yyyy-dd-mm");

		//Create or Update
		Tifs.save(newTif);
		MSG.success("TIF created successfully");
		$state.go('app.tifs');
	};

	$scope.delete = function() {
		MSG.confirm('TIF', "Are you sure you want to delete this TIF?").then(function(confirm) {
			if (confirm) {
				var res = Tifs.delete($scope.tif);
				if (res.success) {
					MSG.success(res.message)
					$state.go('app.tifs')
				} else {
					MSG.error(res.message);
				};
			};
		});
	};

	//Set Reference Number for TIF
	$scope.referenceChange = function() {
		if ($scope.tif) {
			$scope.tif.no = $scope.tif.generateId ? generateReference() : $scope.tif.no;
		};
	};

	//Reset TIF Reference Number when compartment changes
	$scope.$watch('tif.compartment', function() {
		$scope.referenceChange();
	});

	function generateReference() {
		//ForestCode-CompartmentNumber-YYMMDD-HH:MM
		var forestCode = AppConfig.forest().code.substring(0,4);
		var compartmentNumber = $scope.tif.compartment ? $scope.tif.compartment.no : "";
		var d = $filter('date')(new Date(), 'yyMMdd-HH:mm');
		if (compartmentNumber) forestCode += "-" + compartmentNumber;
		return forestCode + "-" + d;
	}

	function start() {
		$scope.offReserve = (AppConfig.forest().type == "Nat.Off");
		
		//Load Data
		fetchData();

		//Set View Title and Tif
		if ($stateParams.id == "new") {
			$scope.title = "Create New TIF";
			$scope.tif = {
				date: DateHelper.toDay(),
				supervisor: AppConfig.supervisor()
			}
		} else {
			$scope.title = "Edit TIF";
			getTif($stateParams.id);
			if (!$scope.tif) {
				$state.go('app.tifs')
			} else {
				$scope.tif.date = DateHelper.formatDate($scope.tif.date);
			}
		};
	}

	start();
})

.controller('TreeCtrl', function($scope, $ionicNavBarDelegate, $ionicModal, $ionicLoading, $filter, AppConfig, DateHelper, $state, $stateParams, Model, MSG, Tifs, Species, Certificates, Compartments, Locations) {

	//Load Species
	$scope.species = Species.all();

	//Adding Tree to TIF
	$scope.addTree = function(tree) {
		if (!$scope.editTree) {
			if (checkDuplicate(tree)) {
				MSG.error("Stock No " + tree.stockNo + " already entered.");
				return;
			};
			tree.lineNo = new Date().getTime();
		};

		tree.rSWspecies = tree.specie.oid;

		($scope.editTree) ? $scope.tif.rSWtreeInformationLine[treeIndex] = tree : $scope.tif.rSWtreeInformationLine.push(tree);

		$scope.tif.totalVolume = calculateTotalVolume();
		Tifs.save($scope.tif);
		$ionicLoading.show({
			template: "Tree Added.",
			duration: 1000
		});
		$scope.tree = {
			branchVolume: 0
		};
		if ($scope.editTree) {
			$ionicNavBarDelegate.back();
		}
	};

	function checkDuplicate(tree) {
		return (_.findWhere($scope.tif.rSWtreeInformationLine, {
			stockNo: tree.stockNo
		}));
	}

	//Removing Tree from TIF
	$scope.removeTree = function() {
		MSG.confirm("Tree", "Are you sure you want to remove this tree").then(function(confirm) {
			if (confirm) {
				$scope.tif.rSWtreeInformationLine.splice(treeIndex, 1);
				$scope.tif.totalVolume = calculateTotalVolume();
				Tifs.save($scope.tif);
				$ionicNavBarDelegate.back();
			};
		});
	};

	$scope.$watch('tree.db1', function() {
		doCalculation();
	});

	$scope.$watch('tree.db2', function() {
		doCalculation();
	});

	$scope.$watch('tree.dt1', function() {
		doCalculation();
	});

	$scope.$watch('tree.dt2', function() {
		doCalculation();
	});

	$scope.$watch('tree.length', function() {
		doCalculation();
	});

	$scope.$watch('tree.branchVolume', function() {
		doCalculation();
	});

	//Adding tree to TIF
	function calculateTotalVolume() {
		var total = 0;
		if ($scope.tif.rSWtreeInformationLine) {
			angular.forEach($scope.tif.rSWtreeInformationLine, function(tree, i) {
				total += parseFloat(tree.volume);
			});
		};
		return $filter('number')(total, 3);
	}

	//Calculate tree volumes
	function doCalculation() {
		if ($scope.tree) {
			//Set default values
			var branchVolume = (!$scope.tree.branchVolume) ? 0 : $scope.tree.branchVolume;


			//Set DB2 Values If 2-Diameter Measurement
			if (!$scope.tree.measurement) {
				$scope.tree.db2 = $scope.tree.db1;
				$scope.tree.dt2 = $scope.tree.dt1;
			};

			//Calculate DB and DT
			$scope.tree.db = (parseFloat($scope.tree.db1) + parseFloat($scope.tree.db2)) / 2;
			$scope.tree.dt = (parseFloat($scope.tree.dt1) + parseFloat($scope.tree.dt2)) / 2;

			//Calculate Volumes
			var boleVolume = (((Math.pow($scope.tree.db, 2) + Math.pow($scope.tree.dt, 2)) * 0.00007854 / 2).toFixed(2)) * parseFloat($scope.tree.length)
			$scope.tree.boleVolume = $filter('number')(boleVolume, 3);
			var volume = parseFloat($scope.tree.boleVolume) + parseFloat(branchVolume);
			$scope.tree.volume = $filter('number')(volume, 3);
		};
	}

	var treeIndex = "";
	function start() {
		$scope.tif = Tifs.findByIndex($stateParams.tif);

		//Set View Title and Tif
		if ($stateParams.id == "new") {
			$scope.title = "Add Tree";
			$scope.editTree = false;
			$scope.tree = {
				branchVolume: 0
			}
		} else {
			$scope.title = "Edit Tree";
			treeIndex = $stateParams.id;
			$scope.tree = $scope.tif.rSWtreeInformationLine[treeIndex];
			$scope.editTree = true;
			if (!$scope.tree) {
				$state.go('app.tifs')
			} else {
				
			}
		};
	}

	start();
})

.controller('HarvestCtrl', function($scope, $ionicLoading, MSG, Certificates, PlantationPermits, Session, AppConfig) {
	$scope.noData = !Certificates.hasData() && !PlantationPermits.hasData();

	$scope.reload = function() {
		var msg = "Are you sure you want to reload data for harvesting?";
		MSG.confirm("Reload", msg).then(function(confirm) {
			if (confirm) {
				doReload();
			};
		});
	}

	function doReload() {
		var work = {
			forest: AppConfig.forest(),
			operation: {
				name: 'Harvesting',
				code: 'Harvest',
				view: 'app.harvest'
			}
		};

		//Pull new certificates for forest
		$ionicLoading.show({
			template: 'Loading haversting data for ' + angular.lowercase(work.forest.name)
		});
		Session.loadOperationalData(work).then(function(res) {
			MSG.success("Data loaded successfully.")
			$ionicLoading.hide();
			$scope.noData = !Certificates.hasData();
		}).catch(function(err) {
			$ionicLoading.hide();
			MSG.error(err);
		});
	}

	if ($scope.noData) {
		doReload();
	};
});