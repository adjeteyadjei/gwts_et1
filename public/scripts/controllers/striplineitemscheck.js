'use strict'
angular.module('MobileApp.controllers')
.controller('striplineItemsCheckCtrl',function($scope,Sync,Species,CheckStripLines,CheckSurvey,CheckStripLineItems,AppConfig,Adapter,$ionicLoading,$ionicModal,$q,Schema,$state,MSG,$stateParams){
	window.items = CheckStripLines;
	window.ss = $scope;
	$scope.species = [];
  $scope.itemTypes = [];
  $scope.condition_scores = [];
  $scope.striplineItems = [];
  $scope.stripline = [];
  $scope.survey = [];
  
  var itemStockNos = [];
  var itemMetresAlong = [];
  
  function start() {
		if($stateParams.type){
			if($stateParams.type == "show"){
				$scope.title = 'View Stripline Item';
				$scope.stripline = CheckStripLines.findByIndex($stateParams.striplineId);
				$scope.striplineItem = CheckStripLineItems.findByIndex($stateParams.itemId);
				console.log($scope.striplineItem);
				
			}else{
				prepareForm();
			}
		}else{
			getStriplineItems()
		}
	}

	start();

  $scope.getItemHeight = function() {
		return 100;
	};

	function getStriplineItems(){
		$scope.striplineItems = [];
	  $scope.stripline = [];
	  $scope.survey = [];
		$scope.stripline = CheckStripLines.findByIndex($stateParams.striplineId);
		$scope.survey = CheckSurvey.find({oid:$scope.stripline.rSWcheckSurvey})[0];
		var items = CheckStripLineItems.find({rSWstripLineCheck:$scope.stripline.oid});
		console.log('striplineitemscheck',items);
		$scope.totalToSync = 0;
		angular.forEach(items,function(item){
			if(item.isDirty){ 
				$scope.totalToSync++;
			}
			item.rSWspecies = getSpecie(item.rSWspecies);
			this.push(item);
		},$scope.striplineItems);
	}

	function getSpecie(oid){
		var specie = Species.find({oid:oid});
		return _.size(specie) > 0 ? specie[0].tradeName : "" ;
	}

	function getSyncables(){

	}

	$scope.isTree = function(item){
		if(!item){var item = $scope.item}
		return _.contains(['Right','Left'],item.type)
	}


	$scope.valStockNo = function(){
		return _.contains(itemStockNos,$scope.item.stockNo)
	}

	function prepareForm(){
		$scope.species = Species.all();
		$scope.suffices = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
		$scope.itemTypes = [
	  	{name: 'Tree Right ->',value:'Right'},
	  	{name: '<- Tree Left',value:'Left'},
	  	{name: 'Condition Score',value:'cond.Score'},
	  	{name: 'Dry Stream',value:'Dry Stream'},
	  	{name: 'Wet Stream (Left -> Right)',value:'WS L/R'},
	  	{name: 'Wet Stream (Right -> Left)',value:'WS R/L'},
	  	{name: 'Road',value:'Road'},
	  	{name: 'Skid Track',value:'Skid Track'},
	  	{name: 'Footpath',value:'Footpath'},
	  	{name: 'Swamp',value:'Swamp'},
	  	{name: 'Rocky Area',value:'Rocky Area'},
	  	{name: 'Slope 15/30',value:'Slope 15/30'},
	  	{name: 'Slope > 30',value:'Slope > 30'}
	  ];
	  $scope.condition_scores = ["0","1","2","3","4","5","6"];

		getStriplineItems();
		$scope.title = 'New Stripline Item';
		
		angular.forEach($scope.striplineItems,function(item){
			itemStockNos.push(item.stockNo);
			itemMetresAlong.push(item.metresAlong);
		});

		$scope.item = {
			type:$scope.itemTypes[0].value,
			stockNo:_.max(itemStockNos) + 1,
			metresAlong:_.max(itemMetresAlong),
			conditionScore:'',
			offset:'',
			dbh:'',
			rSWspecies:'',
			suffix:'',
			rSWstripLineCheck:$scope.stripline.oid,
			canoeTree:false,
			accessIssues:false,
			damaged:false,
			seedTree:false,
			isEdit : false,
			isDirty : true
		}
		// console.log($scope.striplineItems);
		// window.nos = itemStockNos;
		if($stateParams.type == 'edit'){
			$scope.title = 'Edit Stripline Item';
			$scope.item = CheckStripLineItems.findByIndex($stateParams.itemId);
			if(_.size($scope.item) > 0){
				$scope.item.isEdit = true;
				$scope.item.isDirty = true;
			}else{
				MSG.show("Could not find the item you requested for");
		$state.go		('app.striplineItemscheck',{id:$stateParams.striplineId});
			}
		}
		console.log($scope.item);
	}

	function validate(item){
		var res = {status:true,message:[]};
		if(_.contains(itemStockNos,item.no) && $scope.isTree(item)){
			res.status = false;
			res.message.push('Stock Number already exists');
		} 
		if($scope.isTree(item) && item.offset > 30){
			res.status = false;
			res.message.push('Offset cannot be more than 30');
		}
		if(item.type == 'cond.Score' && (item.metresAlong % 60 != 0)){
			res.status = false;
			res.message.push('All condition scores should be at every 60 meters');
		}
		return res;
	}

	$scope.save = function(item){
		var res = validate(item);
		if(res.status){
			if(item.isEdit){
				delete item.isEdit;
				if(!item.isDirty){
					var oldItem = CheckStripLineItems.findByIndex($stateParams.itemId);
					delete oldItem.isDirty
					delete item.isDirty
					item.isDirty = (_.isEqual(item,oldItem)) ? false : true;
				}
			}
			delete item.isEdit;
			if(!$scope.isTree(item)){
				item.stockNo = '';
				item.offset = '';
				item.dbh = '';
				item.rSWspecies = '';
				item.canoeTree = false;
				item.accessIssues = false;
				item.damaged = false;
				item.seedTree = false;
				if(item.type != "cond.Score"){
					item.conditionScore = '';
				}
			}
			CheckStripLineItems.save(item)
			console.log(item);
			MSG.success("Stripline Item saved successfully");
			$state.go('app.striplineItemscheck',{striplineId:$stateParams.striplineId});
		}else{
			// var message = "";
			// angular.forEach(res.message,function(msg){
			// 	message = message + "<br/>";
			// });
			MSG.error(res.message);
		}
	}

	$scope.delete = function(item) {
		console.log(item);
		var res = CheckStripLineItems.delete(item);
		res.success ? MSG.success(res.message) : MSG.error(res.message);
		getStriplineItems();
	};

	$scope.edit = function(id){
		$state.go('app.editStriplineItemcheck',{type:'edit',striplineId:$stateParams.striplineId,itemId:id});
	};

	$scope.show = function(id){
		$state.go('app.showStriplineItemcheck',{type:'show',striplineId:$stateParams.striplineId,itemId:id});
	};

	$scope.new = function(id){
		$state.go('app.newStriplineItemFormcheck',{type:'new',striplineId:id});
	};

	$scope.sync = function() {
		var syncedItems = [];
		var syncErrors = "";
		
		var syncables = [];
		$scope.success_report = "";
		$scope.failure_report = [];
		$scope.syncfinished = false;

		MSG.confirm('Syncing Stripline Items', 'Are you sure you want to send items to the server?').then(function(res) {
			if (res) {
				$scope.modal.show();
				var items = CheckStripLineItems.find({rSWstripLineCheck:$scope.stripline.oid});
				items.forEach(function(item){
					if(item.isDirty){ 
						syncables.push(item);
					}
				}); 
				var count = 0;
				console.log('syncables',syncables);
				angular.forEach(syncables,function(record){
					var stash = angular.copy(record);
					Sync.syncStriplineItemCheck(record).then(function(res){
						syncedItems.push(res);
						count++;
						if(count == syncables.length){
							getStriplineItems();
							$scope.syncfinished = true;
						}
						$scope.success_report = syncedItems.length + " out of " + syncables.length + " records sent to server"
					},function(err){
						$scope.failure_report.push(err);
						count++;
						if(count == syncables.length){
							getStriplineItems();
							$scope.syncfinished = true;
						}
					},function(notify){
						
					});
				});
			}
		});
	};

// Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/sync_report.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the  modal to close it
  $scope.close = function() {
    $scope.modal.hide();
  };

});