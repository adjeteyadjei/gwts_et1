'use strict';

angular.module('MobileApp.controllers')
	.controller('WelcomeCtrl', function($scope, $ionicLoading, Authenticate, Session, MSG, AppConfig, Forests, $state) {
		$scope.operations = [{
			name: 'Harvesting',
			code: 'Harvest',
			view: 'app.harvest'
		}, {
			name: 'Transportation',
			code: 'Harvest', //This is harvest because Ssi take harvest and survey only. Harvest and Transport share same data
			view: 'app.transport'
		}, {
			name: 'Check Point',
			code: 'CheckPoint',
			view: 'app.checkcertificate'
		}, {
			name: 'Stock Survey',
			code: 'Survey',
			view: 'app.survey'
		},{
			name: 'Check Survey',
			code: 'Check',
			view: 'app.check'
		}];

		function getForest() {
			$scope.forests = Forests.all();
		}

		function start() {
			Session.init().then(function(res) {
					getForest();
					$ionicLoading.hide();
				},
				function(err) {
					$ionicLoading.show({
						template: err,
						duration: 3000
					});
				},
				function(note) {
					$ionicLoading.show({
						template: note
					});
				});
		}

		$scope.setWork = function(work) {
			if (Authenticate.secure()) {
				//work.operation = JSON.parse(work.operation);
				if(work.operation.code == "CheckPoint"){
					$state.go(work.operation.view);
					return;
				}
				work.forest = JSON.parse(work.forest);
				var forest = AppConfig.forest();
				if (!forest) forest = AppConfig.forest(work.forest);
				//Check forest change
				if (forest.oid != work.forest.oid) {
					var msg = "You are swtiching forest from " + forest.code + " to " + work.forest.code +
						". Make sure you have synced all data related to " + forest.code +
						" before swtiching to " + work.forest.code + " forest. <br>" +
						"Are you sure you want to swtich to " + work.forest.code + " now?";
					MSG.confirm("Switching Forest", msg).then(function(confirm) {
						if (confirm) {
							//Setting new forest
							AppConfig.forest(work.forest);

							//Pull new certificates for forest
							if (work.operation.code != 'Survey') {
								$ionicLoading.show({
									template: 'Loading ' + angular.lowercase(work.operation.name) + ' data for ' + angular.lowercase(work.forest.name)
								});
								Session.loadOperationalData(work).then(function(res) {
									$ionicLoading.show({
										template: res,
										duration: 3000
									});
									$state.go(work.operation.view);
								}).catch(function(err) {
									$ionicLoading.hide();
									MSG.error(err);
								});
							}
							$state.go(work.operation.view);
						};
					});
				} else {
					$state.go(work.operation.view);
				}
			} else {
				MSG.alert('You are not logged in');
			}
		}
		start();
	});