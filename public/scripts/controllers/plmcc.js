'use strict';

/**
 * @ngdoc function
 * @name MobileApp.controller:PlmccCtrl
 * @author olonka
 * # PlmccCtrl
 * Controller of the MobileApp
 */
angular.module('MobileApp.controllers')
	.controller('PlmccsCtrl', function($q, $scope, Model, $ionicLoading, $ionicModal, MSG, Plmccs, Sync) {
		$scope.plmccs = [];

		function readPlmccs(argument) {
			$scope.plmccs = Plmccs.all();
		}

		readPlmccs();

		//Setting default list item height for collection repeat
		$scope.getItemHeight = function() {
			return 50;
		};

		$scope.delete = function(plmcc) {
			var res = Plmccs.delete(plmcc);
			res.success ? MSG.success(res.message) : MSG.error(res.message);
		};

		function getSyncables() {
			$scope.totalToSync = 0;
			$scope.plmccs.forEach(function(plmcc) {
				if (plmcc.rSWdeliveryDocketLine.length > 0 || plmcc.rSWdeliverySummaryLine.length > 0) {
					$scope.totalToSync++;
				}
			});
		}

		$ionicModal.fromTemplateUrl('templates/sync_report.html', {
			scope: $scope
		}).then(function(modal) {
			$scope.modal = modal;
		});

		$scope.close = function() {
			$scope.modal.hide();
		};

		var syncedPlmccs = [];
		var syncErrors = "";

		function syncData(argument) {
			var count = 0;
			syncErrors = "";
			var d = $q.defer();
			var fieldsToIgnore = ["_id", "_index", "generateId", "permit",
				"destination", "totalVolume", "expireDate", "rSWforestCertCompartment",
				"rSWforestCertificate","rSWdeliveryDocketLine.volume",
				"destination", "rSWdeliveryDocketLine.measurement",
				"rSWdeliveryDocketLine.specie", "rSWdeliveryDocketLine.lineNo",
				"rSWdeliverySummaryLine.specie", "rSWdeliverySummaryLine.lineNo"
			];
			//Sync Report
			$scope.modal.show();
			var syncedItems = 0;
			$scope.report_title = "PLMCCs";
			$scope.success_report = "";
			$scope.failure_report = [];
			$scope.syncfinished = false;
			$scope.plmccs.forEach(function(record) {
				if (record.rSWdeliveryDocketLine.length > 0 || record.rSWdeliverySummaryLine.length > 0) {
					Sync.push('plmcc', record, fieldsToIgnore).then(function(res) {
						count++;
						if (res.status) {
							if (res.data.status) {
								syncedItems++;
								syncedPlmccs.push(record);
								$scope.success_report = syncedItems + " out of " + $scope.totalToSync + " records sent to server";
							} else {
								var err = count + ": " + record.no + " -> " + res.data.errorMessage;
								syncErrors += err;
								$scope.failure_report.push(err);
							}
						}
						if (count == $scope.totalToSync) {
							return d.resolve(res);
							$scope.syncfinished = true;
						}
					}, function(err) {
						count++;
						err = "Reference No (" + record.no + ") -> Error Connecting to server";
						syncErrors += err;
						$scope.failure_report.push(err);
						if (count == $scope.totalToSync) {
							return d.resolve(err);
							$scope.syncfinished = true;
						}
					});
				};

			});
			return d.promise;
		}

		$scope.sync = function() {
			MSG.confirm('Syncing PLMCC', "Are you sure you want to push PLMCCs to the server?").then(function(res) {
				if (res) {
					syncData().then(function(argument) {
						//Delete Synced PLMCCS
						syncedPlmccs.forEach(function(splmcc) {
							Plmccs.delete(splmcc);
						});
						$scope.syncfinished = true;
						start();
					});
				};
			})
		};

		function start() {
			readPlmccs();
			getSyncables();
		}

		start();
	})
	.controller('PlmccCtrl', function($scope, AppConfig, $ionicModal, $stateParams, $filter, $state, Model, $ionicLoading, MSG, DateHelper, Plmccs, PlantationPermits, Locations, Species) {
		$scope.plmcc = {};
		var editMode = false;

		function getPlmcc(index) {
			$scope.plmcc = Plmccs.findByIndex(index);
		}

		//Load all lookup data for operation
		function fetchData() {
			$scope.destinations = Locations.all();
			$scope.permits = PlantationPermits.all();
			$scope.species = Species.all();
		}

		function generateReference() {
			//ForestCode-CompartmentNumber-YYMMDD-HH:MM
			var forestCode = AppConfig.forest().code.substring(0, 4);
			var compartmentNumber = $scope.plmcc.compartment ? $scope.plmcc.compartment.no : "";
			var d = $filter('date')(new Date(), 'yyMMdd-HH:mm');
			if (compartmentNumber) forestCode += "-" + compartmentNumber;
			return forestCode + "-" + d;
		}

		function start() {
			if ($stateParams.id == "new") {
				$scope.title = "Create New PLMCC";
				editMode = false;
				$scope.plmcc = {
					date: DateHelper.toDay(),
					supervisor: AppConfig.supervisor(),
					expireDate: DateHelper.addDay(new Date(), 1)
				}
			} else {
				$scope.title = "Edit PLMCC";
				editMode = true;
				getPlmcc($stateParams.id);
				if (!$scope.plmcc) {
					$state.go('app.plmccs');
				} else {
					$scope.plmcc.date = DateHelper.formatDate($scope.plmcc.date);
				}
			}
			fetchData();
		}

		$scope.referenceChange = function() {
			if (!editMode) {
				$scope.plmcc.no = $scope.plmcc.generateId ? generateReference() : $scope.plmcc.no;
			};
		};

		$scope.$watch('plmcc.compartment', function() {
			$scope.referenceChange();
		});

		$scope.setEntryModel = function(plmcc) {
			Plmccs.save(plmcc);
		}

		$scope.save = function(newPlmcc) {
			AppConfig.supervisor(newPlmcc.supervisor);

			//Error Handling if Certificate not found
			if (!newPlmcc.permit) {
				MSG.error('Please select permit.');
				return;
			};

			//Setting data [contractor,permit,forest,line]
			newPlmcc.rSWdestination = newPlmcc.destination.oid;
			newPlmcc.rSWforestCertificate = newPlmcc.permit.oid;
			newPlmcc.rSWforest = newPlmcc.permit.rSWforest.oid;
			newPlmcc.rSWcontractor = newPlmcc.permit.rSWcontractor.oid;
			if (!newPlmcc.rSWdeliveryDocketLine) {
				newPlmcc.rSWdeliveryDocketLine = [];
				newPlmcc.rSWdeliverySummaryLine = [];
			};

			//Reset Date Format
			newPlmcc.date = DateHelper.formatDate(newPlmcc.date, "yyyy-dd-mm");

			//Create or Update
			var res = Plmccs.save(newPlmcc);
			if (res.success) {
				MSG.success("Plmcc created successfully");
				$state.go('app.plmccs');
			} else {
				MSG.error(res.message);
			};
		};

		$scope.delete = function() {
			MSG.confirm('PLMCC', "Are you sure you want to delete this PLMCC?").then(function(confirm) {
				if (confirm) {
					var res = Plmccs.delete($scope.plmcc);
					if (res.success) {
						MSG.success(res.message)
						$state.go('app.plmccs')
					} else {
						MSG.error(res.message);
					};
				};
			});
		};

		start();
	})
	.controller('PlantLogCtrl', function($scope, AppConfig, $ionicModal, $ionicNavBarDelegate, $stateParams, $filter, $state, Model, $ionicLoading, MSG, DateHelper, Plmccs, Locations, Species) {
		//Load Species
		$scope.species = Species.all();

		$scope.addLog = function(log) {
			if (!$scope.editLog) {
				if (checkDuplicate(log)) {
					MSG.error("Log No " + log.logNo + " already entered for TIF " + log.tIFNo);
					return;
				};
				log.lineNo = new Date().getTime();
			};

			log.rSWspecies = log.specie.oid;

			($scope.editLog) ? $scope.plmcc.rSWdeliveryDocketLine[logIndex] = log : $scope.plmcc.rSWdeliveryDocketLine.push(log);

			$scope.plmcc.totalVolume = calculateTotalVolume();
			Plmccs.save($scope.plmcc);
			$ionicLoading.show({
				template: "Log Added.",
				duration: 1000
			});
			$scope.log = {};
			if ($scope.editLog) {
				$ionicNavBarDelegate.back();
			}
		};

		function checkDuplicate(log) {
			var filter = {
				logNo: log.logNo,
				stockNo: log.stockNo
			};
			var obj = _.findWhere($scope.plmcc.rSWdeliveryDocketLine, filter);
			return (obj);
		}

		$scope.removeLog = function() {
			MSG.confirm("Log", "Are you sure you want to delete this log?").then(function(confirm) {
				if (confirm) {
					$scope.plmcc.rSWdeliveryDocketLine.splice(logIndex, 1);
					$scope.plmcc.totalVolume = calculateTotalVolume();
					Plmccs.save($scope.plmcc);
					$ionicNavBarDelegate.back();
				}
			});
		};

		$scope.$watch('log.dmp', function() {
			doCalculation();
		});

		$scope.$watch('log.length', function() {
			doCalculation();
		});

		function calculateTotalVolume() {
			var total = 0;
			if ($scope.plmcc.rSWdeliveryDocketLine) {
				angular.forEach($scope.plmcc.rSWdeliveryDocketLine, function(log, i) {
					total += parseFloat(log.volume);
				});
			};
			return $filter('number')(total, 3);
		}

		function doCalculation() {
			if ($scope.log) {

				//Calculate Volumes
				var volume = (Math.pow($scope.log.dmp, 2) * 0.00007854) * parseFloat($scope.log.length)
				$scope.log.volume = $filter('number')(volume, 3);
			};
		}

		var logIndex = "";

		function start() {
			$scope.plmcc = Plmccs.findByIndex($stateParams.plmcc);

			if ($stateParams.id == "new") {
				$scope.editLog = false;
				$scope.log = {};
				$scope.title = "Add Log"
			} else {
				$scope.title = "Edit Log";
				logIndex = $stateParams.id;
				$scope.log = $scope.plmcc.rSWdeliveryDocketLine[logIndex];
				$scope.editLog = true;
			};
		}

		start();
	})

.controller('PlantEntryCtrl', function($scope, AppConfig, $ionicModal, $ionicNavBarDelegate, $stateParams, $filter, $state, Model, $ionicLoading, MSG, DateHelper, Plmccs, Locations, Species) {
	//Load Species
	$scope.species = Species.all();

	$scope.addEntry = function(entry) {
		if (!$scope.editEntry) {
			entry.lineNo = new Date().getTime();
		};

		entry.rSWspecies = entry.specie.oid;

		($scope.editEntry) ? $scope.plmcc.rSWdeliverySummaryLine[entryIndex] = entry : $scope.plmcc.rSWdeliverySummaryLine.push(entry);

		$scope.plmcc.totalVolume = calculateTotalVolume();
		Plmccs.save($scope.plmcc);
		$ionicLoading.show({
			template: "Entry Added.",
			duration: 1000
		});
		$scope.entry = {};
		if ($scope.editEntry) {
			$ionicNavBarDelegate.back();
		}
	};

	$scope.removeEntry = function() {
		MSG.confirm("Entry", "Are you sure you want to delete this entry?").then(function(confirm) {
			if (confirm) {
				$scope.plmcc.rSWdeliverySummaryLine.splice(entryIndex, 1);
				$scope.plmcc.totalVolume = calculateTotalVolume();
				Plmccs.save($scope.plmcc);
				$ionicNavBarDelegate.back();
			}
		});
	};

	function calculateTotalVolume() {
		var total = 0;
		if ($scope.plmcc.rSWdeliverySummaryLine) {
			angular.forEach($scope.plmcc.rSWdeliverySummaryLine, function(entry, i) {
				total += parseFloat(entry.volume);
			});
		};
		return $filter('number')(total, 3);
	}

	var entryIndex = "";

	function start() {
		$scope.plmcc = Plmccs.findByIndex($stateParams.plmcc);

		if ($stateParams.id == "new") {
			$scope.editEntry = false;
			$scope.entry = {};
			$scope.title = "Add Entry"
		} else {
			$scope.title = "Edit Entry";
			entryIndex = $stateParams.id;
			$scope.entry = $scope.plmcc.rSWdeliverySummaryLine[entryIndex];
			$scope.editEntry = true;
		};
	}

	start();
});