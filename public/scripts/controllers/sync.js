'use strict';

/**
 * @ngdoc function
 * @name mobileAppApp.controller:SyncCtrl
 * @description
 * # SyncCtrl
 * Controller of the mobileAppApp
 */
angular.module('MobileApp.controllers')
	.controller('SyncCtrl', function($scope, Sync, pouchdb, Model, $ionicLoading, MSG, Store) {

		$scope.synchronize = function(models) {
			if (models) {
				$ionicLoading.show({
					template: 'Syncing...'
				});
				angular.forEach(models, function(i, model) {
					if (model) {
						Sync.pull(model).then(function(res) {
							if (res.status) {
								var data = res.data.data.objects;
								var dbSet = new Store(model);
								dbSet.drop();
								dbSet.post(data);
								$ionicLoading.show({ template: model + ' loaded successfully', duration:2000 });
							};
						},function(err) {
							$ionicLoading.show({ template: 'Could not load '+model + ', Error connecting to the server', duration:2000 });
						});
					};
				});
			$ionicLoading.hide();
			};
		};
	});