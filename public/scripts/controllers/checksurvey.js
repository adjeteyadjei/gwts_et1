'use strict'
angular.module('MobileApp.controllers')
.controller('checkCtrl',function($scope,CheckSurvey,CheckStripLines,CheckStripLineItems,$ionicLoading, MSG,AppConfig,Adapter,$q,Store,Schema){
	window.msg = MSG;
	window.AppConfig = AppConfig;
	window.survey = CheckSurvey;	
	$scope.surveys = [];
	
	function loadSurveysFromDevice(){
		$scope.surveys = CheckSurvey.all();
	}

	$scope.reloadSurvey = function(){
		CheckSurvey.drop();
		fetchSurvey().then(
			function(res){
			console.log("res");
				if(res){
					console.log("Check Survey Reloaded");
					loadSurveysFromDevice();
					$ionicLoading.hide();
				}
			},
			function(err){
				$ionicLoading.show({template: err,duration:3000});
			},
			function(note){
				$ionicLoading.show({template: note});
			}
		);
	}

	function fetchSurvey(){
		var d = $q.defer();
		var surveys = CheckSurvey.all();
		console.log("Surveys",surveys);
		console.log("Surveys Size",_.size(surveys));
		var change = false
		if(surveys.length > 0){
			if(surveys[0].rSWforestCode != AppConfig.getForestCode()) change = true
		}

		if((surveys.length < 1) || change){
			$ionicLoading.show({template: 'Requesting active surveys from server...'});
			var model = 'stock_survey_on_reserve';
			var props = Schema[model].properties; //Represents certificate class properties
      //Filter out only compartments that are not suitable for harvest
      var filter = {
        suitableForHarvest:false,
        completedSurvey:true
      };
      //Get oids for stock surveys with active check surveys
			Adapter.query(model, props, filter).then(function(result){
				if(result.status){
					var data = result.data.data.objects;
					$ionicLoading.show({template: 'processing.......'});
					var oids = [];
	        angular.forEach(data, function(obj, i) {
	        	if(obj.rSWforestCertificate.rSWforest.code === AppConfig.getForestCode()){ //Get TUC for current forest Reserve, NB: this may be buggy
	        		if(obj.rSWcheckSurvey != []){
	        			if(obj.rSWcheckSurveyOid != "") {oids.push(obj.rSWcheckSurveyOid)} 
	        		}
	        	}
	        });
	        console.log("OIDS",oids)
	        console.log("OIDS Size",oids.length)
	        if(oids.length > 0){
	    			$ionicLoading.show({template: 'Requesting active check surveys from server...'});
	    			var model = 'check_survey_on_reserve';
	    			var props = Schema[model].properties; //Represents certificate class properties
	          //Filter out only check surveys that have not been completed
	          var filter = {
	            checkComplete:false
	          };
	          //Pull along the striplines and stripline items for this check survey
		        props['rSWstripLineCheck'] = Schema['check_stripline'].properties;
		        props['rSWstripLineCheck']['rSWstripLineCheckItem'] = Schema['check_stripline_item'].properties;
		        $ionicLoading.show({template: 'Retrieving active check surveys for ' + AppConfig.getForestName()});
		        oids = oids.join();
	      		Adapter.query(model,props,filter,oids).then(function(res){ //make another request for this one but this time bring down the striplines
	      			var result = res.data
	      			if(result.status){
	      				$ionicLoading.show({template: 'Processing Striplines for Check Survey ........'});
	      				var checksurveysData = result.data.objects;
	      				var checksurveys = [];
	      				var striplineItems = [];
	      				var striplines = [];

	      				//Process Striplines
	      				CheckStripLines.drop();
								angular.forEach(checksurveysData,function(survey){
									angular.forEach(survey.rSWstripLineCheckOid,function(stripline){
										stripline.rSWcheckSurvey = stripline.rSWcheckSurvey.oid;
										stripline.isDirty = false;

										//Copy all Stripline Items on this stripline
										angular.forEach(stripline.rSWstripLineCheckItemOid,function(item){
											item.rSWstripLineCheckOid = item.rSWstripLineCheck.oid;
											item.isDirty = false;
											if(item.rSWspecies != undefined){
												item.rSWspecies = item.rSWspecies.oid;
											}
											this.push(item);
										},striplineItems);

										//Remove stripline Items from Stripline::Very important
										delete stripline.rSWstripLineCheckItemOid;
										//Save Stripline to list
										this.push(stripline);
									},striplines);
									delete survey.rSWstripLineCheckOid;
									this.push(survey)
									survey.rSWforestCode = AppConfig.getForestCode();
		      				survey.rSWforestOid = AppConfig.getForestOid();
		      				survey.rSWforestName = AppConfig.getForestName();
								},checksurveys); 

								$ionicLoading.show({template: 'Finalising Check Surveys........'});
	      				CheckSurvey.drop();
	      				CheckSurvey.post(checksurveys);
	      				$ionicLoading.show({template: 'Check Surveys saved on device'});

	      				$ionicLoading.show({template: 'Finalising striplines for Check Survey ........'});
	      				CheckStripLines.drop();
	      				CheckStripLines.post(striplines);
	      				$ionicLoading.show({template: 'Striplines for Check Survey saved on device'});

	      				$ionicLoading.show({template: 'Finalising Stripline Items for Check Survey........'});
	      				CheckStripLineItems.drop();

	      				CheckStripLineItems.post(striplineItems);
	      				$ionicLoading.show({template: 'Stripline Items for Check Survey saved on device'});
	      				d.resolve(true);

	      			}else{
	      				$ionicLoading.show({template: result.errorMessage,duration:3000});
	      				d.reject();
	      			}
	      		},function(err){
	      			$ionicLoading.show({template: 'Error connecting to the server',duration:3000});
							d.reject()
						});
	        }else{
	        	//No active check survey found
	        	$ionicLoading.show({template: 'No active Check Survey available for '+ AppConfig.getForestName(),duration:3000});
						d.reject();
	        }
				}else{
					$ionicLoading.show({template: 'faulty query',duration:3000});
					d.reject();
				}
			},function(err){
				$ionicLoading.show({template: 'Error connecting to the server',duration:3000});
				d.reject()
			});
		}else{
			d.resolve(true);
		}
		return d.promise;
	}

	fetchSurvey().then(
		function(res){
			if(res){
				loadSurveysFromDevice();
				$ionicLoading.hide();
			}
		});
});