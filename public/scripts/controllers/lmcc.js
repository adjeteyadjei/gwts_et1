'use strict';

/**
 * @ngdoc function
 * @name MobileApp.controller:LmccCtrl
 * @author olonka
 * # LmccCtrl
 * Controller of the MobileApp
 */
angular.module('MobileApp.controllers')
	.controller('LmccsCtrl', function($q, $scope, Model, $ionicLoading, $ionicModal, MSG, Lmccs, Sync) {
		$scope.lmccs = [];

		function readLmccs(argument) {
			$scope.lmccs = Lmccs.all();
		}

		readLmccs();

		//Setting default list item height for collection repeat
		$scope.getItemHeight = function() {
			return 50;
		};

		$scope.delete = function(lmcc) {
			var res = Lmccs.delete(lmcc);
			res.success ? MSG.success(res.message) : MSG.error(res.message);
		};

		function getSyncables() {
			$scope.totalToSync = 0;
			$scope.lmccs.forEach(function(lmcc) {
				if (lmcc.rSWdeliveryDocketLine.length > 0) {
					$scope.totalToSync++;
				}
			});
		}

		$ionicModal.fromTemplateUrl('templates/sync_report.html', {
			scope: $scope
		}).then(function(modal) {
			$scope.modal = modal;
		});

		$scope.close = function() {
			$scope.modal.hide();
		};

		var syncedLmccs = [];
		var syncErrors = "";

		function syncData(argument) {
			var count = 0;
			syncErrors = "";
			var d = $q.defer();
			var fieldsToIgnore = ["_id", "_index", "generateId", "certificate",
				"compartment", "totalVolume", "expireDate", "rSWforestCertCompartment",
				"rSWforestCertificate", "rSWdeliveryDocketLine.db", "rSWdeliveryDocketLine.dt",
				"rSWdeliveryDocketLine.volume", "destination", "rSWdeliveryDocketLine.measurement",
				"rSWdeliveryDocketLine.totalVolume", "rSWdeliveryDocketLine.specie", "rSWdeliveryDocketLine.lineNo"
			];
			//Sync Report
			$scope.modal.show();
			var syncedItems = 0;
			$scope.report_title = "LMCCs";
			$scope.success_report = "";
			$scope.failure_report = [];
			$scope.syncfinished = false;
			$scope.lmccs.forEach(function(record) {
				if (record.rSWdeliveryDocketLine.length > 0) {
					Sync.push('lmcc', record, fieldsToIgnore).then(function(res) {
						count++;
						if (res.status) {
							if (res.data.status) {
								syncedItems++;
								syncedLmccs.push(record);
								$scope.success_report = syncedItems + " out of " + $scope.totalToSync + " records sent to server";
							} else {
								var err = count + ": " + record.no + " -> " + res.data.errorMessage;
								syncErrors += err;
								$scope.failure_report.push(err);
							}
						}
						if (count == $scope.totalToSync) {
							return d.resolve(res);
							$scope.syncfinished = true;
						}
					}, function(err) {
						count++;
						err = "Reference No (" + record.no + ") -> Error Connecting to server";
						syncErrors += err;
						$scope.failure_report.push(err);
						if (count == $scope.totalToSync) {
							return d.resolve(err);
							$scope.syncfinished = true;
						}
					});
				};

			});
			return d.promise;
		}

		$scope.sync = function() {
			MSG.confirm('Syncing LMCC', "Are you sure you want to push LMCCs to the server?").then(function(res) {
				if (res) {
					syncData().then(function(argument) {
						//Delete Synced LMCCS
						syncedLmccs.forEach(function(slmcc) {
							Lmccs.delete(slmcc);
						});
						$scope.syncfinished = true;
						start();
					});
				};
			})
		};

		function start() {
			readLmccs();
			getSyncables();
		}

		start();
	})
	.controller('LmccCtrl', function($scope, AppConfig, $ionicModal, $stateParams, $filter, $state, Model, $ionicLoading, MSG, DateHelper, Lmccs, Certificates, Compartments, Locations, Species) {
		$scope.lmcc = {};
		var editMode = false;

		function getLmcc(index) {
			$scope.lmcc = Lmccs.findByIndex(index);
		}

		//Load all lookup data for operation
		function fetchData() {
			$scope.mills = Locations.find({
				isMill: true
			});
			$scope.compartments = Compartments.all();
			$scope.certificates = Certificates.all();
			$scope.species = Species.all();
		}

		function generateReference() {
			//ForestCode-CompartmentNumber-YYMMDD-HH:MM
			var forestCode = AppConfig.forest().code.substring(0,4);
			var compartmentNumber = $scope.lmcc.compartment ? $scope.lmcc.compartment.no : "";
			var d = $filter('date')(new Date(), 'yyMMdd-HH:mm');
			if (compartmentNumber) forestCode += "-" + compartmentNumber;
		return forestCode + "-" + d;
		}

		function start() {
			$scope.offReserve = (AppConfig.forest().type == "Nat.Off");
			
			if ($stateParams.id == "new") {
				$scope.title = "Create New LMCC";
				editMode = false;
				$scope.lmcc = {
					date: DateHelper.toDay(),
					supervisor: AppConfig.supervisor(),
					expireDate: DateHelper.addDay(new Date(), 1)
				}
			} else {
				$scope.title = "Edit LMCC";
				editMode = true;
				getLmcc($stateParams.id);
				if (!$scope.lmcc) {
					$state.go('app.lmccs');
				} else {
					$scope.lmcc.date = DateHelper.formatDate($scope.lmcc.date);
				}
			}
			fetchData();
		}

		$scope.referenceChange = function() {
			if (!editMode) {
				$scope.lmcc.no = $scope.lmcc.generateId ? generateReference() : $scope.lmcc.no;
			};
		};

		$scope.$watch('lmcc.compartment', function() {
			$scope.referenceChange();
		});

		$scope.save = function(newLmcc) {
			var certificate = {};
			AppConfig.supervisor(newLmcc.supervisor);
			if (AppConfig.forest().type == "Nat.On") {
				//On Reserve LMCC.
				certificate = Certificates.find({
					oid: newLmcc.compartment.rSWforestCertificate.oid
				})[0];

				//On Reserve LMCC requires rSWforestCertCompartment and rSWcompartment
				newLmcc.rSWforestCertCompartment = newLmcc.compartment.oid;
				newLmcc.rSWcompartment = newLmcc.compartment.rSWcompartment.oid;

			} else {
				//Off Reserve LMCC
				certificate = newLmcc.certificate;
			};

			//Error Handling if Certificate not found
			if (!certificate.oid) {
				MSG.error('Forest certificate of the selected compartment not found.' +
					' Please goto home view, select operation and sync data with server.');
				return;
			};

			//Setting common data [contractor,certificate,forest,line]
			newLmcc.certificate = certificate;
			newLmcc.rSWdestination = newLmcc.destination.oid;
			newLmcc.rSWforestCertificate = certificate.oid;
			newLmcc.rSWforest = certificate.rSWforest.oid;
			newLmcc.rSWcontractor = certificate.rSWcontractor.oid;
			if (!newLmcc.rSWdeliveryDocketLine) {
				newLmcc.rSWdeliveryDocketLine = [];
			};

			//Reset Date Format
			newLmcc.date = DateHelper.formatDate(newLmcc.date, "yyyy-dd-mm");

			//Create or Update
			var res = Lmccs.save(newLmcc);
			if (res.success) {
				MSG.success("Lmcc created successfully");
				$state.go('app.lmccs');
			} else {
				MSG.error(res.message);
			};
		};

		$scope.delete = function() {
			MSG.confirm('LMCC', "Are you sure you want to delete this LMCC?").then(function(confirm) {
				if (confirm) {
					var res = Lmccs.delete($scope.lmcc);
					if (res.success) {
						MSG.success(res.message)
						$state.go('app.lmccs')
					} else {
						MSG.error(res.message);
					};
				};
			});
		};

		start();
	})
	.controller('LogCtrl', function($scope, AppConfig, $ionicModal, $ionicNavBarDelegate, $stateParams, $filter, $state, Model, $ionicLoading, MSG, DateHelper, Lmccs, Locations, Species) {
		//Load Species
		$scope.species = Species.all();
		
		$scope.addLog = function(log) {
			if (!$scope.editLog) {
				if (checkDuplicate(log)) {
					MSG.error("Log No " + log.logNo + " already entered for TIF " + log.tIFNo);
					return;
				};
				log.lineNo = new Date().getTime();
			};

			log.rSWspecies = log.specie.oid;

			($scope.editLog) ? $scope.lmcc.rSWdeliveryDocketLine[logIndex] = log : $scope.lmcc.rSWdeliveryDocketLine.push(log);

			$scope.lmcc.totalVolume = calculateTotalVolume();
			Lmccs.save($scope.lmcc);
			$ionicLoading.show({
				template: "Log Added.",
				duration: 1000
			});
			$scope.log = {};
			if ($scope.editLog) {
				$ionicNavBarDelegate.back();
			}
		};

		function checkDuplicate(log) {
			var filter = {
				logNo: log.logNo,
				stockNo: log.stockNo
			};
			console.log($scope.lmcc);
			var obj = _.findWhere($scope.lmcc.rSWdeliveryDocketLine, filter);
			return (obj);
		}

		$scope.removeLog = function() {
			MSG.confirm("Log", "Are you sure you want to delete this log?").then(function(confirm) {
				if (confirm) {
					$scope.lmcc.rSWdeliveryDocketLine.splice(logIndex, 1);
					$scope.lmcc.totalVolume = calculateTotalVolume();
					Lmccs.save($scope.lmcc);
					$ionicNavBarDelegate.back();
				}
			});
		};

		$scope.$watch('log.db1', function() {
			doCalculation();
		});

		$scope.$watch('log.db2', function() {
			doCalculation();
		});

		$scope.$watch('log.dt1', function() {
			doCalculation();
		});

		$scope.$watch('log.dt2', function() {
			doCalculation();
		});

		$scope.$watch('log.length', function() {
			doCalculation();
		});

		$scope.$watch('log.branchVolume', function() {
			doCalculation();
		});

		function calculateTotalVolume() {
			var total = 0;
			if ($scope.lmcc.rSWdeliveryDocketLine) {
				angular.forEach($scope.lmcc.rSWdeliveryDocketLine, function(log, i) {
					total += parseFloat(log.volume);
				});
			};
			return $filter('number')(total, 3);
		}

		function doCalculation() {
			if ($scope.log) {
				//Set DB2 Values If 2-Diameter Measurement
				if (!$scope.log.measurement) {
					$scope.log.db2 = $scope.log.db1;
					$scope.log.dt2 = $scope.log.dt1;
				};

				//Calculate DB and DT
				$scope.log.db = (parseFloat($scope.log.db1) + parseFloat($scope.log.db2)) / 2;
				$scope.log.dt = (parseFloat($scope.log.dt1) + parseFloat($scope.log.dt2)) / 2;

				//Calculate Volumes
				var volume = (((Math.pow($scope.log.db, 2) + Math.pow($scope.log.dt, 2)) * 0.00007854 / 2).toFixed(2)) * parseFloat($scope.log.length);
				$scope.log.volume = $filter('number')(volume, 3);
			};
		}

		var logIndex = "";
		function start() {
			$scope.lmcc = Lmccs.findByIndex($stateParams.lmcc);

			if ($stateParams.id == "new") {
				$scope.editLog = false;
				$scope.log = {};
				$scope.title = "Add Log"
			} else {
				$scope.title = "Edit Log";
				logIndex = $stateParams.id;
				$scope.log = $scope.lmcc.rSWdeliveryDocketLine[logIndex];
				$scope.editLog = true;
			};
		}

		start();
	})
	.controller('TransportCtrl', function($scope, $ionicLoading, MSG, Certificates, PlantationPermits, Session, AppConfig) {
		$scope.noData = !Certificates.hasData() && !PlantationPermits.hasData();

		$scope.reload = function() {
			var msg = "Are you sure you want to reload data for transportation?";
			MSG.confirm("Reload", msg).then(function(confirm) {
				if (confirm) {
					doReload();
				};
			});
		}

		function doReload() {
			var work = {
				forest: AppConfig.forest(),
				operation: {
					name: 'Transportation',
					code: 'Harvest',
					view: 'app.harvest'
				}
			};
			//Pull new certificates for forest
			$ionicLoading.show({
				template: 'Loading transportation data for ' + angular.lowercase(work.forest.name)
			});
			Session.loadOperationalData(work).then(function(res) {
				MSG.success("Data loaded successfully.")
				$ionicLoading.hide();
				$scope.noData = !Certificates.hasData();
			}).catch(function(err) {
				$ionicLoading.hide();
				MSG.error(err);
			});
		}

		if ($scope.noData) {
			doReload();
		};
	});