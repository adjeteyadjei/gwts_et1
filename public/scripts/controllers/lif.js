'use strict';

/**
 * @ngdoc function
 * @name MobileApp.controller:LifCtrl
 * @author olonka
 * # LifCtrl
 * Controller of the MobileApp
 */
angular.module('MobileApp.controllers')
	.controller('LifsCtrl', function($q, $scope, Model, $ionicLoading, $ionicModal, MSG, Lifs, Sync) {
		$scope.lifs = [];

		function readLifs(argument) {
			$scope.lifs = Lifs.all();
		}
		readLifs();

		//Setting default list item height for collection repeat
		$scope.getItemHeight = function() {
			return 50;
		};

		$scope.delete = function(lif) {
			var res = Lifs.delete(lif);
			res.success ? MSG.success(res.message) : MSG.error(res.message);
		};

		function getSyncables() {
			$scope.totalToSync = 0;
			$scope.lifs.forEach(function(lif) {
				if (lif.rSWlogInformationLine.length > 0) {
					$scope.totalToSync++;
				}
			});
		}

		$ionicModal.fromTemplateUrl('templates/sync_report.html', {
			scope: $scope
		}).then(function(modal) {
			$scope.modal = modal;
		});

		$scope.close = function() {
			$scope.modal.hide();
		};

		var syncedLifs = [];
		var syncErrors = "";

		function syncData(argument) {
			var count = 0;
			syncErrors = "";
			var d = $q.defer();
			var fieldsToIgnore = ["_id", "_index", "generateId", "certificate",
				"compartment", "totalVolume", "expireDate", "rSWforestCertCompartment",
				"rSWforestCertificate", "rSWlogInformationLine.db", "rSWlogInformationLine.dt",
				"rSWlogInformationLine.volume", "destination", "rSWlogInformationLine.measurement",
				"rSWlogInformationLine.totalVolume", "rSWlogInformationLine.specie",
				"rSWlogInformationLine.lineNo"
			];
			//Sync Report
			$scope.modal.show();
			var syncedItems = 0;
			$scope.report_title = "LIFs";
			$scope.success_report = "";
			$scope.failure_report = [];
			$scope.syncfinished = false;
			$scope.lifs.forEach(function(record) {
				if (record.rSWlogInformationLine.length > 0) {
					Sync.push('lif', record, fieldsToIgnore).then(function(res) {
						count++;
						if (res.status) {
							if (res.data.status) {
								syncedItems++;
								syncedLifs.push(record);
								$scope.success_report = syncedItems + " out of " + $scope.totalToSync + " records sent to server";
							} else {
								var err = count + ": " + record.no + " -> " + res.data.errorMessage;
								syncErrors += err;
								$scope.failure_report.push(err);
							}
						}
						if (count == $scope.totalToSync) {
							return d.resolve(res);
							$scope.syncfinished = true;
						}
					}, function(err) {
						count++;
						err = "Reference No (" + record.no + ") -> Error Connecting to server";
						syncErrors += err;
						$scope.failure_report.push(err);
						if (count == $scope.totalToSync) {
							return d.resolve(err);
							$scope.syncfinished = true;
						}
					});
				};
			});
			return d.promise;
		}

		$scope.sync = function() {
			MSG.confirm('Syncing LIF', "Are you sure you want to push LIFs to the server?").then(function(res) {
				if (res) {
					syncData().then(function(argument) {
						//Delete Synced LIFS
						syncedLifs.forEach(function(slif) {
							Lifs.delete(slif);
						});
						$scope.syncfinished = true;
						start();
					});
				};
			})
		};

		function start() {
			readLifs();
			getSyncables();
		}

		start();
	})

.controller('LifCtrl', function($scope, AppConfig, $ionicModal, $stateParams, $filter, $state, Model, $ionicLoading, MSG, DateHelper, Lifs, Certificates, Compartments, Locations, Species, Contractors) {
	$scope.lif = {};
	var editMode = false;

	function getLif(index) {
		$scope.lif = Lifs.findByIndex(index);
	}

	//Load all lookup data for operation
	function fetchData() {
		$scope.mills = Locations.find({
			isMill: true
		});

		$scope.contractors = Contractors.all();
		$scope.compartments = Compartments.all();
		$scope.certificates = Certificates.all();
		$scope.species = Species.all();
	}

	function generateReference() {
		//ForestCode-CompartmentNumber-YYMMDD-HH:MM
		var forestCode = AppConfig.forest().code;
		var compartmentNumber = $scope.lif.compartment ? $scope.lif.compartment.no : "";
		var d = $filter('date')(new Date(), 'yyMMdd-HH:mm');
		return forestCode + "-" + compartmentNumber + "-" + d;
	}

	function start() {
		if ($stateParams.id == "new") {
			$scope.title = "Create New LIF";
			editMode = false;
			$scope.lif = {
				date: DateHelper.toDay(),
				supervisor: AppConfig.supervisor(),
				expireDate: DateHelper.addDay(new Date(), 1)
			}
		} else {
			$scope.title = "Edit LIF";
			editMode = true;
			getLif($stateParams.id);
			if (!$scope.lif) {
				$state.go('app.lifs')
			} else {
				$scope.lif.date = DateHelper.formatDate($scope.lif.date);
			}
		}
		fetchData();
	}

	$scope.referenceChange = function() {
		if (!editMode) {
			$scope.lif.no = $scope.lif.generateId ? generateReference() : $scope.lif.no;
		};
	};

	$scope.$watch('lif.compartment', function() {
		$scope.referenceChange();
	});

	//Defining log model form
	$ionicModal.fromTemplateUrl('templates/lif/log.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.logForm = modal;
	});

	$scope.closeLogForm = function() {
		$scope.logForm.hide();
	};

	$scope.editLog = false;
	var logIndex = "";
	$scope.showLogForm = function(log, i) {
		if (log) {
			$scope.log = log;
			logIndex = i;
			$scope.editLog = true;
			$scope.title = "Edit Log";
		} else {
			$scope.log = {
				db1: 0,
				db2: 0,
				dt1: 0,
				dt2: 0,
				volume: 0
			};
			$scope.title = "Add Log"
		};
		$scope.logForm.show();
	};

	$scope.addLog = function(log) {
		if (checkDuplicate(log)) {
			MSG.error("Log No " + log.logNo + " already entered for TIF " + log.tIFNo);
			return;
		};
		log.lineNo = new Date().getTime();
		log.specie = jQuery.parseJSON(log.specie);
		log.rSWspecies = log.specie.oid;
		$scope.lif.rSWlogInformationLine.push(log);
		$scope.lif.totalVolume = calculateTotalVolume();
		Lifs.save($scope.lif);
		$scope.closeLogForm();
	};

	function checkDuplicate(log) {
		var filter = {
			logNo: log.logNo,
			stockNo: log.stockNo
		};
		var obj = _.findWhere($scope.lif.rSWlogInformationLine, filter);
		return (obj);
	}

	$scope.removeLog = function() {
		$scope.lif.rSWlogInformationLine.splice(logIndex, 1);
		$scope.lif.totalVolume = calculateTotalVolume();
		Lifs.save($scope.lif);
		$scope.closeLogForm();
	};

	$scope.$watch('log.db1', function() {
		doCalculation();
	});

	$scope.$watch('log.db2', function() {
		doCalculation();
	});

	$scope.$watch('log.dt1', function() {
		doCalculation();
	});

	$scope.$watch('log.dt2', function() {
		doCalculation();
	});

	$scope.$watch('log.length', function() {
		doCalculation();
	});

	$scope.$watch('log.branchVolume', function() {
		doCalculation();
	});

	function calculateTotalVolume() {
		var total = 0;
		if ($scope.lif.rSWlogInformationLine) {
			angular.forEach($scope.lif.rSWlogInformationLine, function(log, i) {
				total += parseFloat(log.volume);
			});
		};
		return $filter('number')(total, 3);
	}

	function doCalculation() {
		if ($scope.log) {
			//Set DB2 Values If 2-Diameter Measurement
			if (!$scope.log.measurement) {
				$scope.log.db2 = $scope.log.db1;
				$scope.log.dt2 = $scope.log.dt1;
			};

			//Calculate DB and DT
			$scope.log.db = (parseFloat($scope.log.db1) + parseFloat($scope.log.db2)) / 2;
			$scope.log.dt = (parseFloat($scope.log.dt1) + parseFloat($scope.log.dt2)) / 2;

			//Calculate Volumes
			var volume = (((Math.pow($scope.log.db, 2) + Math.pow($scope.log.dt, 2)) * 0.00007854 / 2).toFixed(2)) * parseFloat($scope.log.length);
			$scope.log.volume = $filter('number')(volume, 3);
		};
	}

	$scope.save = function(newLif) {
		var certificate = {};
		AppConfig.supervisor(newLif.supervisor);
		if (AppConfig.forest().type == "Nat.On") {
			//On Reserve LMCC.
			certificate = Certificates.find({
				oid: newLif.compartment.rSWforestCertificate.oid
			})[0];

			//On Reserve LMCC requires rSWforestCertCompartment and rSWcompartment
			newLif.rSWforestCertCompartment = newLif.compartment.oid;
			newLif.rSWcompartment = newLif.compartment.rSWcompartment.oid;

		} else {
			//Off Reserve LMCC
			certificate = newLif.certificate;
		};

		//Error Handling if Certificate not found
		if (!certificate.oid) {
			MSG.error('Forest certificate of the selected compartment not found.' +
				' Please goto home view, select operation and sync data with server.');
			return;
		};

		//Setting common data [contractor,certificate,forest,line]
		newLif.certificate = certificate;
		newLif.rSWdestination = newLif.destination.oid;
		newLif.rSWforestCertificate = certificate.oid;
		newLif.rSWforest = certificate.rSWforest.oid;
		newLif.rSWcontractor = certificate.rSWcontractor.oid;
		if (!newLif.rSWlogInformationLine) {
			newLif.rSWlogInformationLine = [];
		};

		//Reset Date Format
		newLif.date = DateHelper.formatDate(newLif.date, "yyyy-dd-mm");

		//Create or Update
		var res = Lifs.save(newLif);
		if (res.success) {
			MSG.success("Lif created successfully");
			$state.go('app.lifs');
		} else {
			MSG.error(res.message);
		};
	};

	$scope.delete = function() {
		MSG.confirm('LIF', "Are you sure you want to delete this LIF?").then(function(confirm) {
			if (confirm) {
				var res = Lifs.delete($scope.lif);
				if (res.success) {
					MSG.success(res.message)
					$state.go('app.lifs')
				} else {
					MSG.error(res.message);
				};
			};
		});

	};

	start();
});