'use strict'
angular.module('MobileApp.controllers')
.controller('striplinesCheckCtrl',function($scope,Sync,storage,CheckSurvey,CheckStripLines,CheckStripLineItems,$ionicModal,$ionicLoading,AppConfig,Adapter,$q,Schema,$state, $stateParams,MSG){
	window.strip = CheckStripLines;
	window.survey = CheckSurvey;
	window.storage = storage;
	window.state = $state;

	$scope.striplines = [];
	$scope.survey = [];
	$scope.title = '';
	var syncables = []; 


	var striplineNumbers = [];
	var editStripline = {};

	function start() { 
		if($stateParams.type){
			if($stateParams.type == "show"){
				$scope.title = 'View Stripline';
				$scope.survey = CheckSurvey.find({oid:$stateParams.surveyId})
				$scope.stripline = CheckStripLines.findByIndex($stateParams.id);
			}else{
				prepareForm();
			}
		}else{
			getStriplines()
		}
	}

	start();

	function getStriplines(){
		$scope.survey = CheckSurvey.find({oid:$stateParams.id})[0];
		$scope.striplines = CheckStripLines.find({rSWcheckSurvey:$stateParams.id});
		getSyncables();
		console.log(syncables);
	}

	function getSyncables(){
		var items = CheckStripLineItems.all();
		$scope.totalToSync = 0;
		angular.forEach(items,function(item){
			if(item.isDirty){ 
				$scope.totalToSync++;
				this.push(item);
			} 
			
		},syncables);
	}

	function prepareForm(){
		$scope.title = 'New Stripline';
		$scope.stripline = {
			rSWCheckSurvey : $stateParams.surveyId,
			LineNo : "",
			oid : "",
			directions : ['A->B','B->A'],
			isDirty : true,
			isEdit : false
		}
		angular.forEach($scope.striplines,function(line){
			this.push(line.no);
		},striplineNumbers);
		if($stateParams.type == "edit"){
			$scope.title = 'Edit Stripline';
			editStripline = CheckStripLines.findByIndex($stateParams.id);
			if(_.size(editStripline) > 0){
				$scope.stripline = editStripline;
				$scope.stripline.isEdit = true;
			}else{
				MSG.show("Could not find the Stripline you requested for");
				$state.go('app.striplines',{id:$stateParams.surveyId});
			}
		}
	}

	function validate(stripline){
		var res = {status:true,message:''};
		if(_.contains(striplineNumbers,stripline.no)){
			res.status = false;
			res.message = "Stripline Already Exists";
		} 
		return res;
	}

	$scope.save = function(stripline){
		var val = validate(stripline);
		if(val.status){
			console.log(stripline);
			if(stripline.isEdit){
				if(_.isEqual(stripline,editStripline)){
					stripline.isDirty = false;
				}
			}
			delete stripline.isEdit;
			var resId = CheckStripLines.save(stripline)._id;
			var data = CheckStripLines.findById(resId);
			data.oid = resId;
			CheckStripLines.save(data);
			console.log(data);
			MSG.success("Stripline saved successfully");
			$state.go('app.striplines',{id:$stateParams.surveyId});
		}else{
			MSG.error(val.message);
		}
	}

	$scope.delete = function(stripline) {
		var res = CheckStripLines.delete(stripline);
		res.success ? MSG.success(res.message) : MSG.error(res.message);
		getStriplines();
	};

	$scope.edit = function(surveyId,id){
		$state.go('app.editStriplineform',{type:'edit',surveyId:$scope.survey.oid,id:id});
	};

	$scope.show = function(surveyId,id){
		$state.go('app.showStriplineform',{type:'show',surveyId:$scope.survey.oid,id:id});
	};

	$scope.new = function(id){
		id = parseFloat(id).toFixed(3); //this is risky because we dont know when the id format will change
		$state.go('app.newStriplineForm',{type:'new',surveyId:id});
	};

	function updateSyncedRecord(record){
		console.log('Record after sync',record);
		var data = CheckStripLineItems.findById(record._id);
		data.isDirty = false;
		CheckStripLineItems.save(data);
		console.log('Record after saved',data);
	}

	// Sync Center
$scope.sync = function() {
		var syncedItems = [];
		var syncErrors = "";
		
		var syncables = [];
		$scope.report_title = "Stock Survey";
		$scope.success_report = "";
		$scope.failure_report = [];
		$scope.syncfinished = false;

		MSG.confirm('Syncing Stock Survey', 'Are you sure you want to send items to the server?').then(function(res) {
			if (res) {
				$scope.modal.show();
				var items = CheckStripLineItems.all();
				angular.forEach(items,function(item){
					if(item.isDirty){ 
						this.push(item);
					}
				},syncables); 
				var count = 0;
				angular.forEach(syncables,function(record){
					var stash = angular.copy(record);
					Sync.syncStriplineItem(record).then(function(res){
						syncedItems.push(res);
						count++;
						if(count == syncables.length){
							getStriplines();
							$scope.syncfinished = true;
						}
						$scope.success_report = syncedItems.length + " out of " + syncables.length + " records sent to server";
					},function(err){
						$scope.failure_report.push(err);
						count++;
						if(count == syncables.length){
							getStriplines();
							$scope.syncfinished = true;
						}
					},function(notify){
						
					});
				});
			}
		});
	};

// Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/sync_report.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the  modal to close it
  $scope.close = function() {
    $scope.modal.hide();
  };

});