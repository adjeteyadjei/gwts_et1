'use strict';

/**
 * @ngdoc function
 * @name MobileApp.controller:PpcCtrl
 * @description
 * # PpcCtrl
 * Controller of the MobileApp
 */
angular.module('MobileApp.controllers')
  .controller('PpcsCtrl', function($scope, $q, Model, $ionicLoading, $ionicModal, MSG, Ppcs, Sync) {
		$scope.ppcs = [];

		function readPpcs() {
			$scope.ppcs = Ppcs.all();
		}

		//Setting default list item height for collection repeat
		$scope.getItemHeight = function() {
			return 50;
		};

		$scope.delete = function(ppc) {
			MSG.confirm('PPC', "Are you sure you want to delete this PPC?").then(function(confirm) {
				if (confirm) {
					var res = Ppcs.delete(ppc);
					res.success ? MSG.success(res.message) : MSG.error(res.message);
				};
			});
		};

		function getSyncables() {
			$scope.totalToSync = 0;
			$scope.ppcs.forEach(function(ppc) {
				if (ppc.rSWplantationProductionLine.length > 0) {
					$scope.totalToSync++;
				}
			});
		}

		$ionicModal.fromTemplateUrl('templates/sync_report.html', {
			scope: $scope
		}).then(function(modal) {
			$scope.modal = modal;
		});

		$scope.close = function() {
			$scope.modal.hide();
		};

		var syncedPpcs = [];
		var syncErrors = "";

		function syncData(argument) {
			var count = 0;
			syncErrors = "";
			var d = $q.defer();
			var fieldsToIgnore = ["_id", "_index", "generateId", "permit", "specie",
				"harvestArea", "totalVolume", "rSWplantationProductionLine.boleVolume",
				"rSWplantationProductionLine.volume", "rSWplantationProductionLine.totalVolume",
				"rSWplantationProductionLine.specie", "rSWplantationProductionLine.measurement",
				"rSWplantationProductionLine.lineNo"
			];
			//Sync Report
			$scope.modal.show();
			var syncedItems = 0;
			$scope.report_title = "PPCs";
			$scope.success_report = "";
			$scope.failure_report = [];
			$scope.syncfinished = false;
			$scope.ppcs.forEach(function(record) {
				if (record.rSWplantationProductionLine.length > 0) {
					Sync.push('ppc', record, fieldsToIgnore).then(function(res) {
						count++;
						if (res.status) {
							if (res.data.status) {
								syncedItems++;
								syncedPpcs.push(record);
								$scope.success_report = syncedItems + " out of " + $scope.totalToSync + " records sent to server";
							} else {
								var err = count + ": " + record.no + " -> " + res.data.errorMessage;
								syncErrors += err;
								$scope.failure_report.push(err);
							}
						}
						if (count == $scope.totalToSync) {
							return d.resolve(res);
							$scope.syncfinished = true;
						}
					});
				};

			});
			return d.promise;
		}

		$scope.sync = function() {
			MSG.confirm('Syncing PPC', "Are you sure you want to push PPCs to the server?").then(function(res) {
				if (res) {
					syncData().then(function(argument) {
						//Delete Synced PPCS
						syncedPpcs.forEach(function(sppc) {
							Ppcs.delete(sppc);
						});
						$scope.syncfinished = true;
						start();
					});
				};
			})
		};

		function start() {
			readPpcs();
			getSyncables();
		}

		start();

	})


.controller('PpcCtrl', function($scope, $ionicModal, $ionicLoading, $filter, AppConfig, DateHelper, $state, $stateParams, Model, MSG, Ppcs, PlantationPermits, Species) {
	$scope.ppc = {};

	function getPpc(index) {
		$scope.ppc = Ppcs.findByIndex(index);
	}

	//Load all lookup data for operation
	function fetchData(argument) {
		$scope.permits = PlantationPermits.all();
		$scope.species = Species.all();
	}

	$scope.save = function(newPpc) {
		AppConfig.supervisor(newPpc.supervisor);

		//Error Handling if Certificate not found
		if (!newPpc.permit) {
			MSG.error('Please select permit.');
			return;
		};

		//Setting data [contractor,permit,forest,line]
		newPpc.rSWforestCertificate = newPpc.permit.oid;
		newPpc.rSWlocation = newPpc.permit.rSWlocation.oid;
		newPpc.rSWcontractor = newPpc.permit.rSWcontractor.oid;
		if (!newPpc.rSWplantationProductionLine) {
			newPpc.rSWplantationProductionLine = [];
		};


		//Reset Date Format
		newPpc.date = DateHelper.formatDate(newPpc.date, "yyyy-dd-mm");

		//Create or Update
		Ppcs.save(newPpc);
		MSG.success("PPC created successfully");
		$state.go('app.ppcs');
	};

	$scope.delete = function() {
		MSG.confirm('PPC', "Are you sure you want to delete this PPC?").then(function(confirm) {
			if (confirm) {
				var res = Ppcs.delete($scope.ppc);
				if (res.success) {
					MSG.success(res.message)
					$state.go('app.ppcs')
				} else {
					MSG.error(res.message);
				};
			};
		});
	};

	//Set Reference Number for PPC
	$scope.referenceChange = function() {
		if ($scope.ppc) {
			$scope.ppc.no = $scope.ppc.generateId ? generateReference() : $scope.ppc.no;
		};
	};

	//Reset PPC Reference Number when harvestArea changes
	$scope.$watch('ppc.harvestArea', function() {
		$scope.referenceChange();
	});

	function generateReference() {
		//ForestCode-CompartmentNumber-YYMMDD-HH:MM
		var forestCode = AppConfig.forest().code.substring(0,4);
		var harvestArea = $scope.ppc.harvestArea ? $scope.ppc.harvestArea.area : "";
		var d = $filter('date')(new Date(), 'yyMMdd-HH:mm');
		if (harvestArea) forestCode += "-" + harvestArea;
		return forestCode + "-" + d;
	}

	function start() {
		//Load Data
		fetchData();

		//Set View Title and Ppc
		if ($stateParams.id == "new") {
			$scope.title = "Create New PPC";
			$scope.ppc = {
				date: DateHelper.toDay(),
				supervisor: AppConfig.supervisor()
			}
		} else {
			$scope.title = "Edit PPC";
			getPpc($stateParams.id);
			if (!$scope.ppc) {
				$state.go('app.ppcs')
			} else {
				$scope.ppc.date = DateHelper.formatDate($scope.ppc.date);
			}
		};
	}

	start();
})

.controller('PlogCtrl', function($scope, $ionicNavBarDelegate, $ionicModal, $ionicLoading, $filter, AppConfig, DateHelper, $state, $stateParams, Model, MSG, Ppcs, Certificates, Compartments, Locations) {

	//Adding Log to PPC
	$scope.addLog = function(log) {
		if (!$scope.editLog) {
			if (checkDuplicate(log)) {
				MSG.error("Contractor No " + log.contractorNo + " already entered.");
				return;
			};
			log.lineNo = new Date().getTime();
		};

		log.rSWspecies = $scope.ppc.specie.oid;
		log.logNo = log.contractorNo;

		($scope.editLog) ? $scope.ppc.rSWplantationProductionLine[logIndex] = log : $scope.ppc.rSWplantationProductionLine.push(log);

		$scope.ppc.totalVolume = calculateTotalVolume();
		Ppcs.save($scope.ppc);
		$ionicLoading.show({
			template: "Log Added.",
			duration: 1000
		});
		$scope.log = {};
		if ($scope.editLog) {
			$ionicNavBarDelegate.back();
		}
	};

	function checkDuplicate(log) {
		return (_.findWhere($scope.ppc.rSWplantationProductionLine, {
			contractorNo: log.contractorNo
		}));
	}

	//Removing Log from PPC
	$scope.removeLog = function() {
		MSG.confirm("Log", "Are you sure you want to remove this log").then(function(confirm) {
			if (confirm) {
				$scope.ppc.rSWplantationProductionLine.splice(logIndex, 1);
				$scope.ppc.totalVolume = calculateTotalVolume();
				Ppcs.save($scope.ppc);
				$ionicNavBarDelegate.back();
			};
		});
	};

	$scope.$watch('log.dmp', function() {
		doCalculation();
	});

	$scope.$watch('log.length', function() {
		doCalculation();
	});

	//Adding log to PPC
	function calculateTotalVolume() {
		var total = 0;
		if ($scope.ppc.rSWplantationProductionLine) {
			angular.forEach($scope.ppc.rSWplantationProductionLine, function(log, i) {
				total += parseFloat(log.volume);
			});
		};
		return $filter('number')(total, 3);
	}

	//Calculate log volumes
	function doCalculation() {
		if ($scope.log) {

			//Calculate Volumes
			var volume = (Math.pow($scope.log.dmp, 2) * 0.00007854) * parseFloat($scope.log.length)
			$scope.log.volume = $filter('number')(volume, 3);
		};
	}

	var logIndex = "";
	function start() {
		$scope.ppc = Ppcs.findByIndex($stateParams.ppc);

		//Set View Title and Ppc
		if ($stateParams.id == "new") {
			$scope.title = "Add Log";
			$scope.editLog = false;
			$scope.log = {}
		} else {
			$scope.title = "Edit Log";
			logIndex = $stateParams.id;
			$scope.log = $scope.ppc.rSWplantationProductionLine[logIndex];
			$scope.editLog = true;
			if (!$scope.log) {
				$state.go('app.ppcs')
			} else {
				
			}
		};
	}

	start();
})